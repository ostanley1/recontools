Local gitlab-runner
===================
Due to how submodules are handled, `cpplapack.git` must be available locally in `../cpplapack.git` as a bare repository.

```bash
git clone --bare cpplapack ../cpplapack.git
```

It is also necessary that `../` be available within the docker image using `--docker-volumes`

```sh
pushd .. && MYLOCATION=`pwd` && popd && gitlab-runner exec docker --cache-dir /cache --docker-volumes `pwd`/ci:/cache --docker-volumes ${MYLOCATION}:${MYLOCATION} test
```

