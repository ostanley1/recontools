//
//  Support.h
//  IceSvd
//
//  Created by Martyn Klassen on 2016-04-18.
//  Copyright © 2016 CFMM. All rights reserved.
//
#ifndef RECONTOOLS_SUPPORT_HPP
#define RECONTOOLS_SUPPORT_HPP

#include <vector>
#include <cmath>
#include <ostream>
#include "recontools/config.hpp"

namespace recontools
{
   namespace fit
   {

      struct RECONTOOLS_API P3
      {
         typedef double value_type;

         value_type r;
         value_type c;
         value_type z;

         P3()
               : r(0.0), c(0.0), z(0.0)
         {}

         P3(const value_type &R, const value_type &C, const value_type &Z)
               : r(R), c(C), z(Z)
         {}

         P3(const P3 &p)
               : r(p.r), c(p.c), z(p.z)
         {}

         explicit P3(const value_type *p)
               : r(p[0]), c(p[1]), z(p[2])
         {}

         value_type len2()
         {
            return r * r + c * c + z * z;
         }

         value_type len()
         {
            return sqrt(len2());
         }

         void assign(value_type *pos)
         {
            pos[0] = r;
            pos[1] = c;
            pos[2] = z;
         }

         P3 &operator=(const P3 &p)
         {
            r = p.r;
            c = p.c;
            z = p.z;
            return *this;
         }

         P3 &operator*=(const value_type &v)
         {
            r *= v;
            c *= v;
            z *= v;
            return *this;
         }

         P3 &operator/=(const value_type &v)
         {
            r /= v;
            c /= v;
            z /= v;
            return *this;
         }

         P3 &operator+=(const P3 &p)
         {
            r += p.r;
            c += p.c;
            z += p.z;
            return *this;
         }

         P3 &operator-=(const P3 &p)
         {
            r -= p.r;
            c -= p.c;
            z -= p.z;
            return *this;
         }

         bool operator==(const value_type &v) const
         {
            return (r == v) && (c == v) && (z == v);
         }

         bool operator==(const P3 &rhs) const
         {
            return (r == rhs.r) && (c == rhs.c) && (z == rhs.z);
         }

         bool operator!=(const P3 &rhs) const
         {
            return (r != rhs.r) || (c != rhs.c) || (z != rhs.z);
         }

         friend P3 operator+(const P3 &lhs, const P3 &rhs)
         {
            return P3(lhs.r + rhs.r, lhs.c + rhs.c, lhs.z + rhs.z);
         }

         friend P3 operator-(const P3 &lhs, const P3 &rhs)
         {
            return P3(lhs.r - rhs.r, lhs.c - rhs.c, lhs.z - rhs.z);
         }

         friend value_type operator*(const P3 &p1, const P3 &p2)
         {
            return p1.r * p2.r + p1.c * p2.c + p1.z * p2.z;
         }

         friend P3 operator*(const P3 &lhs, const value_type &v)
         {
            return P3(lhs.r * v, lhs.c * v, lhs.z * v);
         }

         friend P3 operator*(const value_type &v, const P3 &rhs)
         {
            return P3(rhs.r * v, rhs.c * v, rhs.z * v);
         }

         friend P3 cross(const P3 &p1, const P3 &p2)
         {
            return P3(p1.c * p2.z - p1.z * p2.c,
                      p1.z * p2.r - p1.r * p2.z,
                      p1.r * p2.c - p1.c * p2.r);
         }

         friend bool operator<(const P3 &a, const P3 &b)
         {
            if (a.z == b.z)
            {
               if (a.r == b.r)
               {
                  return a.c < b.c;
               }
               return a.r < b.r;
            }
            return a.z < b.z;
         }

         friend std::ostream &operator<<(std::ostream &stream, const P3 &a)
         {
            return stream << "(" << a.r << ", " << a.c << ", " << a.z << ")";
         }
      };


      class RECONTOOLS_API Support
      {
      public:
         typedef P3::value_type value_type;
         typedef std::vector<bool> mask_type;
         typedef std::vector<value_type> data_type;
         typedef std::vector<int> index_type;

         Support(size_t dims = 3)
         {
            dimensions(dims);
         }


         virtual ~Support()
         {}

         virtual void computePerimeter(const mask_type &mask, const int &nDim, const int *dims, const value_type *center, const value_type *voxel) = 0;

         virtual bool interior(value_type *pos) = 0;

         const data_type &center() const
         { return m_vOffset; }

         virtual bool write(std::ostream &stream);

         virtual bool read(std::istream &stream);

         bool operator==(Support const &rhs)
         {
            return this->dimensions() == rhs.dimensions();
         }

      protected:
         virtual void setupWork(size_t numPoints) = 0;

         virtual void setPosition(value_type *offset) = 0;

         size_t configure(const mask_type &mask, const int &nDim, const int *dims, const value_type *center, const value_type *voxel);

         static void removeColinear(mask_type::iterator const &vertices, int d, const int *dimVertex, const int *incrVertex);

         static void match(mask_type::iterator vertexMask, mask_type::const_iterator iterVoxel, const int *voxelDim, const int *voxelIncr, const int *vertexIncr);

         static void colinear(mask_type::iterator const &mask, const int &dim, const int &incr);

         static void colinearLoop(mask_type::iterator mask, const int &apply, int current, const int *dim, const int *incr);

         void calculatePosition(value_type *position, const value_type *offset, value_type factor = 1.0) const;

         void populate(mask_type::iterator mask, int *dim, int *incr, value_type *offset);

         size_t dimensions() const
         { return m_vOffset.size(); }

         void dimensions(size_t dims);

      private:
         data_type m_vVoxel;
         data_type m_vOffset;
      };
   }
}

#endif //RECONTOOLS_SUPPORT_HPP
