//
// Created by Martyn Klassen on 2019-12-06.
//

#ifndef RECONTOOLS_B1MAP_HPP
#define RECONTOOLS_B1MAP_HPP

#include <complex>
#include <sstream>
#include <recontools/fit/ConvexHull.hpp>
#include <recontools/fit/SolidHarmonics.hpp>
#include <cpplapack/cpplapack.h>
#include <cstring>

namespace recontools
{
   namespace fit
   {
      template<class basis_type=SolidHarmonic<double>, class V=ConvexHull>
      class B1Fit
      {
      public:
         typedef V validity_type;
         typedef typename std::complex<typename basis_type::value_type> value_type;
         typedef typename std::vector<value_type> data_type;

         explicit B1Fit(size_t order = 5, typename basis_type::result_type arg = typename basis_type::result_type(0.0))
               : m_oBasis(order, arg)
               , m_iCoils(0)
         {}

         virtual ~B1Fit()
         {}

         bool operator==(B1Fit<basis_type, validity_type> const &rhs)
         {
            return m_oBasis == rhs.m_oBasis &&
                   m_oValidity == rhs.m_oValidity &&
                   m_vCoefficients == rhs.m_vCoefficients &&
                   m_iCoils == rhs.m_iCoils;
         }

         void coefficients(size_t coils, const value_type *coefficient, size_t lda = 0)
         {
            m_iCoils = coils;
            size_t nBasis = this->setup();
            if (!lda) lda = nBasis;

            value_type const *pInput = coefficient;
            value_type *pOutput = &(m_vCoefficients.front());

            for (size_t i = 0; i < coils; i++)
            {
               std::memcpy(pOutput, pInput, sizeof(*pInput) * nBasis);
               pInput += lda;
               pOutput += nBasis;
            }
         }

         size_t coils() const
         { return m_iCoils; };

         const value_type *coefficients() const
         { return &(m_vCoefficients.front()); }

         virtual void compute(P3 const &pos, typename basis_type::result_type *data)
         {
            m_oBasis.compute(pos, data);
         }

         const data_type &data(P3 &pos)
         {
            // Move to closest location within the support
            m_oValidity.closest(pos);

            // Get the value of the basis at the location
            m_oBasis.compute(pos, m_vBasis);

            // Compute the matrix-vector multiplication
            // Coefficients^T * basis = coil maps
            // (bBasis x nCoils)^T * (nBasis x 1) = (nCoils x 1)
            // (nCoils x nBasis) * (nBasis x 1) = (nCoils x 1)
            size_t nBasis = m_vBasis.size();
            cpplapack::gemv('T', nBasis, m_iCoils, 1.0, m_vCoefficients, nBasis, m_vBasis, 1ul, 0.0, m_vData, 1ul);

            return m_vData;
         }

         const basis_type &basis() const
         { return m_oBasis; }

         const validity_type &validity() const
         { return m_oValidity; }


         virtual bool write(std::ostream &stream)
         {
            if (!m_oBasis.write(stream) || !m_oValidity.write(stream))
            {
               return false;
            }
            stream.write(reinterpret_cast<const char *>(&m_iCoils), sizeof(m_iCoils));
            stream.write(reinterpret_cast<const char *>(&(m_vCoefficients.front())), m_vCoefficients.size() * sizeof(value_type));
            return true;
         }

         virtual bool read(std::istream &stream)
         {
            if (!m_oBasis.read(stream) || !m_oValidity.read(stream))
            {
               return false;
            }
            stream.read(reinterpret_cast<char *>(&m_iCoils), sizeof(m_iCoils));
            this->setup();
            stream.read(reinterpret_cast<char *>(&(m_vCoefficients.front())), m_vCoefficients.size() * sizeof(value_type));
            return true;
         }

      protected:
         virtual bool mask(typename validity_type::mask_type const &mask, int const &ndim, int const *dims,
               typename validity_type::value_type const *center, typename validity_type::value_type const *voxel)
         {
            m_oValidity.computePerimeter(mask, ndim, dims, center, voxel);
            return true;
         }

      private:
         size_t setup()
         {
            size_t nBasis = m_oBasis.terms();
            m_vCoefficients.resize(m_iCoils * nBasis);
            m_vData.resize(m_iCoils);
            m_vBasis.resize(nBasis);
            return nBasis;
         }

         data_type m_vData;
         data_type m_vBasis;

         basis_type m_oBasis;
         validity_type m_oValidity;

         size_t m_iCoils;
         data_type m_vCoefficients;
      };

   }
}

#endif //RECONTOOLS_B1MAP_HPP
