//
// Created by Martyn Klassen on 2019-12-06.
//

#include "Ellipsoid.hpp"
#include "cpplapack.h"
#include <stdexcept>
#include <string>
#include <limits>
#include <cstring>
#include <cstddef>

namespace recontools
{
   namespace fit
   {
      // Sum the vector minimizing overflow
      inline double sum(double *pV, int n)
      {
         if (n <= 1)
            return *pV;
         else
         {
            int first = n / 2;
            return sum(pV, first) + sum(pV + first, n - first);
         }
      }


      void Ellipsoid::compute(value_type *Q, size_t nValue)
      {
         int n = static_cast<int>(nValue);
         int d = static_cast<int>(this->dimensions());
         int i, j;
         int m = d + 1;

         value_type *Q2 = new value_type[n * m + n + m * m];

         try
         {
            value_type *U = Q2 + m * n;
            value_type *X = U + n;

            value_type *pQ, *pQ2, *pU;

            // Error is sum (res * res)
            // If res is eps then error is N * eps * eps
            value_type tolerance = sqrt(std::numeric_limits<value_type>::epsilon() * 0.5);

            value_type err = 1.0;

            value_type value;
            value_type factor;
            value_type maxM;
            ptrdiff_t maxJ(0);

            // Initialize the u vector
            pU = U;
            value = 1.0 / n;
            for (i = 0; i < n; i++)
            {
               *pU++ = value;
            }

            // Perform Khachiyan Algorithm
            while (err > tolerance)
            {
               // STEP 1: X = Q * diag(u) * Q'

               // Q2 = Q * diag(u)'
               pQ2 = Q2;
               pQ = Q;
               pU = U;
               for (i = 0; i < n; i++)
               {
                  for (j = 0; j < m; j++)
                  {
                     *pQ2++ = *pQ++ * *pU;
                  }
                  pU++;
               }
               // X = Q2 * Q';
               cpplapack::gemm('N', 'T', m, m, n, 1.0, Q2, m, Q, m, 0.0, X, m);

               // STEP 2: M = diag(Q' * inv(X) * Q)

               // If u_i > 0 then diag(U) is positive definite and X is positive definite
               // Then inv(X) is positive definite and M is positive definite
               // According to the Nima Moshtagh paper M is positive definite
               // Then inv(X) must also be positive definite
               // This is also what permits the maxM to only consider positive values
               // Assume the u_i > 0.0

               // inv(X) = inv(L)' * inv(L)
               // X = L * L'
               // Q2 = inv(L) * Q
               // M = Q2' * Q2;

               // Solve
               // L * Q2 =  Q
               // Q2 = inv(L) * Q
               memcpy(Q2, Q, sizeof(*Q) * m * n);
               cpplapack::potrf('L', m, X, m);
               cpplapack::trsm('L', 'L', 'N', 'N', m, n, 1.0, X, m, Q2, m);

               // Q2 = inv(X) * Q
               // Solve A * X = B where A = X and B = Q
               // X = Q2 = inv(X) * Q;
               // Note that this is more expensive and requires Q' * Q2 to be evaluated
               // not Q2' * Q2 as is currently calculated
               // ipiv[m] is also not allocated
               //            memcpy(Q2, Q, sizeof(*Q)*m*n);
               //            cpplapack::gesv(m, n, X, m, ipiv, Q2, m);

               // Find maximum in M
               pQ2 = Q2;
               maxM = std::numeric_limits<value_type>::min();
               for (i = 0; i < n; i++)
               {
                  // Compute i-th diagonal of M = Q' * Q2;
                  // Note that Q = Q and not Q', therefore Q needs to be transposed
                  // Which actually makes the computation simpler and faster
                  value = 0.0;
                  pQ = pQ2 + m;
                  while (pQ2 < pQ)
                  {
                     value += *pQ2 * *pQ2;
                     pQ2++;
                  }

                  // Check for maximum
                  if (value > maxM)
                  {
                     maxM = value;
                     maxJ = i;
                  }
               }

               // Calculate the step size
               factor = -((maxM - m) / (m * (maxM - 1)));

               // Compute the next step and the error
               err = 0.0;
               pU = U;
               for (i = 0; i < maxJ; i++)
               {
                  value = *pU * factor;
                  err += value * value;
                  *pU += value;
                  pU++;
               }

               // maxJ point has special value
               // u_next = u + factor * (u - e_maxJ)
               // e_maxJ is unit vector along j, i.e 1 for maxJ and zero elsewhere
               value = factor * (*pU - 1.0);
               err += value * value;
               *pU += value;
               pU++;

               for (i++; i < n; i++)
               {
                  value = *pU * factor;
                  err += value * value;
                  *pU += value;
                  pU++;
               }
            }

            // A = (1/d) * inv(P * U * P' - (P * u) * (P * u)' )
            // A = inv(d * (P * U * P' - (P * u) * (P * u)' ))

            // Q2 = P * u
            cpplapack::gemv('N', d, n, 1.0, Q, m, U, 1, 0.0, Q2, 1);

            // This is the center point
            m_vCenter.resize(static_cast<size_t>(d));
            memcpy(&(m_vCenter.front()), Q2, sizeof(value_type) * m_vCenter.size());

            // X = (P * u) * (P * u)';
            cpplapack::gemm('N', 'T', d, d, 1, 1.0, Q2, d, Q2, d, 0.0, X, d);

            // Q2 = P * diag(u) = P * U
            pU = U;
            pQ = Q;
            pQ2 = Q2;
            for (i = 0; i < n; i++)
            {
               for (j = 0; j < d; j++)
               {
                  *pQ2++ = *pQ++ * *pU;
               }

               // Skip the last point in column
               pQ++;
               pQ2++;

               pU++;
            }

            // X = d * (Q2 * P' - X)
            cpplapack::gemm('N', 'T', d, d, n, d, Q2, m, Q, m, -d, X, d);

            // Ellipsoid is given by (p - c)^T * A * (p - c) <= 1
            // A = L * L^T (Cholesky factorization)
            // (p - c)^T * L * L^T * (p - c) <= 1
            // || L^T * (p - c) || <= 1
            // Need to find L^T of A
            // Perform the Cholesky factorization of inv(A)
            // inv(A) = inv(L)^T * inv(L)
            // Solve for inv(L)
            // A must be symmetric positive definite and therefore inv(A) also
            cpplapack::potrf('L', d, X, d);

            // Store the data
            m_vLinverse.resize(static_cast<size_t>(d * d));
            memcpy(&(m_vLinverse.front()), X, sizeof(value_type) * m_vLinverse.size());
         }
         catch (...)
         {
            delete[] Q2;
            throw;
         }
         delete[] Q2;
      }

      void Ellipsoid::setupWork(size_t numPoints)
      {
         if (!m_pWork)
            throw std::runtime_error("Work not assigned");

         m_pWork->reserve(numPoints * (this->dimensions() + 1));
      }

      void Ellipsoid::setPosition(value_type *offset)
      {
         size_t d = this->dimensions();
         m_pWork->resize(m_pWork->size() + d + 1, 1.0);
         calculatePosition(&(m_pWork->back()) - d, offset, 1.0);
      }

      // Solve for the minimum volume enclosing ellipsoid
      void Ellipsoid::computePerimeter(const mask_type &mask, const int &nDim, const int *dims, const value_type *center, const value_type *voxel)
      {
         data_type Q;
         m_pWork = &Q;

         size_t n = configure(mask, nDim, dims, center, voxel);

         compute(&(Q.front()), n);
      }

      Ellipsoid::data_type Ellipsoid::A() const
      {
         size_t d = this->dimensions();
         data_type a(d * d * 2, 0.0);

         const value_type *L = &(m_vLinverse.front());
         value_type *pA = &(a.front());
         value_type *pL = pA + d * d;

         for (size_t i = 0; i < d; i++)
         {
            *pL = 1.0;
            pL += d + 1;
         }
         pL = pA + d * d;


         cpplapack::trsm('L', 'L', 'N', 'N', d, d, 1.0, L, d, pL, d);
         cpplapack::gemm('N', 'T', d, d, d, 1.0, pL, d, pL, d, 0.0, pA, d);
         a.resize(d * d);

         return a;
      }

      bool Ellipsoid::interior(value_type *pos, const value_type &inflation)
      {
         int i;

         int d = static_cast<int>(this->dimensions());
         value_type *L = &(m_vLinverse.front());

         // Remove the center from the position
         for (i = 0; i < d; i++)
         {
            pos[i] -= m_vCenter[i];
         }

         // Need to compute
         // L^T * (p - c)
         // where E = L * L^T

         // trsm can solve A * x = b for x
         // Let A = Li, where inv(E) = Li * Li^T = inv(L)^T * inv(L)
         // Li * x = b
         // inv(L)^T * x = b
         // x = L^T * b
         // let b = p - c
         // then x = L^T * (p - c)
         // Solve for x
         cpplapack::trsm('L', 'L', 'N', 'N', d, 1, 1.0, L, d, pos, d);

         // Calculate || L^T * (p - c) ||
         value_type value(0.0);
         for (i = 0; i < d; i++)
         {
            value += pos[i] * pos[i];
         }

         // Check if we are inside an ellipsoid inflated by inflation
         return inflation >= value;
      }

      bool Ellipsoid::write(std::ostream &stream)
      {
         if (!Support::write(stream))
            return false;

         RECONTOOLS_UINT32 version = 1;
         stream.write(reinterpret_cast<char const *>(&version), sizeof(version));

         stream.write(reinterpret_cast<char const *>(RECONTOOLS_VECTOR_POINTER(m_vLinverse)), sizeof(m_vLinverse.front()) * m_vLinverse.size());
         stream.write(reinterpret_cast<char const *>(RECONTOOLS_VECTOR_POINTER(m_vCenter)), sizeof(m_vCenter.front()) * m_vCenter.size());
         return true;
      }

      bool Ellipsoid::read(std::istream &stream)
      {
         if (!Support::read(stream))
            return false;

         RECONTOOLS_UINT32 version;
         stream.read(reinterpret_cast<char *>(&version), sizeof(version));

         size_t dims = this->dimensions();
         m_vLinverse.resize(dims * dims, 0.0);
         m_vCenter.resize(dims, 0.0);

         stream.read(reinterpret_cast<char *>(RECONTOOLS_VECTOR_POINTER(m_vLinverse)), sizeof(m_vLinverse.front()) * m_vLinverse.size());
         stream.read(reinterpret_cast<char *>(RECONTOOLS_VECTOR_POINTER(m_vCenter)), sizeof(m_vCenter.front()) * m_vCenter.size());

         return true;
      }
   }
}
