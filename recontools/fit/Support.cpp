//
//  Support.cpp
//  IceSvd
//
//  Created by Martyn Klassen on 2016-04-18.
//  Copyright © 2016 CFMM. All rights reserved.
//

#include "Support.hpp"
#include "cpplapack.h"
#include <iostream>
#include <cstring>

namespace recontools
{
   namespace fit
   {

      void Support::dimensions(size_t dims)
      {
         m_vVoxel.assign(dims * dims, 0.0);
         m_vOffset.assign(dims, 0.0);
         for (size_t i = 0; i < dims; i++)
            m_vVoxel[i + i * dims] = 1.0;
      }

      void Support::match(mask_type::iterator vertexMask, mask_type::const_iterator iterVoxel, const int *voxelDim, const int *voxelIncr, const int *vertexIncr)
      {
         // Each vertex needs to check whether any of the eight adjacent voxels
         // are active. vertexMask is one larger in each dimension than voxelMask
         // and voxelDim is the dimensions of voxelMask. The end voxelDim is
         // denoted by 0
         int i;

         if (*voxelIncr != *vertexIncr)
         {
            // vertexMask if pointing to the vertex above the voxel in iterVoxel

            // Loop through all the voxel dim
            for (i = 0; i < *voxelDim; i++)
            {
               // Compare the vertex at the top of the voxel for all other dimensions
               match(vertexMask, iterVoxel, voxelDim - 1, voxelIncr - 1, vertexIncr - 1);

               // Move the vertex down one plane to below the voxel
               vertexMask += *vertexIncr;

               // Compare the vertex at bottom of the voxel for all other dimensions
               match(vertexMask, iterVoxel, voxelDim - 1, voxelIncr - 1, vertexIncr - 1);

               // Move to the next voxel plane, i.e. below the vertex plane
               iterVoxel += *voxelIncr;
            }
         }
         else
         {
            // The increment is only equal when it is 1, which is the last dimension in the loop
            for (i = 0; i < *voxelDim; i++)
            {
               *vertexMask = *vertexMask | *iterVoxel;
               vertexMask++;
               *vertexMask = *vertexMask | *iterVoxel++;
            }
         }
      }

      void Support::colinear(mask_type::iterator const &mask, const int &dim, const int &incr)
      {
         // Offsets are used instead of iterators because MSVC 2008 does not allow iterator to point
         // beyond 1 past the end of the data.

         // Set all colinear points to false except the first and last points
         // This removes a lot of interior points from further consideration
         // as all the interior points would, at best, be on an edge of the
         // convex hull
         std::size_t maxOffset = static_cast<size_t>(dim * incr);
         std::size_t offset;

         for (offset = 0; (offset < maxOffset) && (!(*(mask + offset))); offset += incr);

         // If there is only one more valid point there is nothing more to do
         if (offset >= (maxOffset - 2 * incr))
            return;

         // Currently offset pointing to the first true point

         // Move to the next point, and set it as previous
         // It does not matter whether it is true or false. It will be set to
         // false if another true point is colinear, or it will be left as is,
         // if no other points are true
         offset += incr;
         mask_type::iterator prev = mask + offset;

         offset += incr;
         while (offset < maxOffset)
         {
            if (*(mask + offset))
            {
               *prev = false;
               prev = mask + offset;
            }
            offset += incr;
         }
      }

      void Support::colinearLoop(mask_type::iterator mask, const int &apply, int current, const int *dim, const int *incr)
      {
         // Skip the dimension to be applied
         if (apply == current) current--;

         if (current >= 0)
         {
            // Loop through all dimensions
            const int offset = incr[current];
            mask_type::iterator end = mask + dim[current] * offset;
            while (mask < end)
            {
               colinearLoop(mask, apply, current - 1, dim, incr);
               mask += offset;
            }
         }
         else
         {
            // Apply colinear test to selected dimension
            colinear(mask, dim[apply], incr[apply]);
         }
      }


      void Support::populate(mask_type::iterator mask, int *dim, int *incr, value_type *offset)
      {
         if (*incr == 1)
         {
            for (int i = 0; i < *dim; i++, mask++)
            {
               if (*mask)
               {
                  *offset = static_cast<double>(i);
                  setPosition(offset);
               }
            }
         }
         else
         {
            for (int i = 0; i < *dim; i++, mask += *incr)
            {
               *offset = static_cast<double>(i);
               populate(mask, dim - 1, incr - 1, offset - 1);
            }
         }
      }

      void Support::removeColinear(mask_type::iterator const &vertices, const int d, const int *dimVertex, const int *incrVertex)
      {
         // Search through the vertices and delete co-linear points
         // If the endpoints of colinear points are within the minimum volume ellipsoid, then
         // all interior points will also be within the minimum volume ellipsoid.

         // Vertices matrix is one larger than voxel in all dimensions
         for (int i = 0; i < d; i++)
         {
            colinearLoop(vertices, i, d - 1, dimVertex, incrVertex);
         }
      }

      void Support::calculatePosition(value_type *position, const value_type *offset, value_type factor) const
      {
         // Calculate the position
         // voxel * (-0.5 * size) + origin
         // voxel hold the voxel edge vectors for each dimension
         int dim = static_cast<int>(dimensions());
         memmove(position, &(m_vOffset.front()), sizeof(value_type) * dim);
         cpplapack::gemv('N', dim, dim, factor,
                         &(m_vVoxel.front()), dim, offset, 1, 1.0, position, 1);
      }

      size_t Support::configure(const mask_type &mask, const int &nDim, const int *dims, const value_type *center, const value_type *voxel)
      {
         m_vVoxel.resize(nDim * nDim);
         m_vOffset.resize(nDim);

         memcpy(&(m_vOffset.front()), center, sizeof(value_type) * m_vOffset.size());
         memcpy(&(m_vVoxel.front()), voxel, sizeof(value_type) * m_vVoxel.size());

         index_type incrVoxel(nDim, 1);
         index_type incrVertex(nDim, 1);
         index_type dimVertex(nDim);

         std::vector<value_type> position(nDim, 0.0);

         // Compute the offset from the center and the total number of vertices
         size_t nVertices = 1;
         size_t i;
         for (i = 0; i < static_cast<size_t>(nDim); i++)
         {
            position[i] = static_cast<value_type>(dims[i]);
            dimVertex[i] = dims[i] + 1;
            nVertices *= dimVertex[i];
            // Compute the increments for voxels and vertices
            if (i)
            {
               incrVertex[i] = dimVertex[i - 1] * incrVertex[i - 1];
               incrVoxel[i] = dims[i - 1] * incrVoxel[i - 1];
            }
         }

         calculatePosition(&(m_vOffset.front()), &(position.front()), -0.5);

         // Allocate memory for vertices
         mask_type vertices(nVertices, false);

         // Tag all vertices adjacent to mask voxel
         match(vertices.begin(), mask.begin(), dims + nDim - 1, &(incrVoxel.back()), &(incrVertex.back()));

         removeColinear(vertices.begin(), nDim, &(dimVertex.front()), &(incrVertex.front()));

         // Count the number of true vertices
         nVertices = 0;
         mask_type::const_iterator iterVertex = vertices.begin();
         while (iterVertex < vertices.end())
         {
            nVertices += *iterVertex++;
         }

         if (nVertices < (static_cast<size_t>(nDim) + 1))
         {
            throw std::runtime_error("Not enough vertices");
         }

         this->setupWork(nVertices);
         populate(vertices.begin(), &(dimVertex.back()), &(incrVertex.back()), &(position.back()));

         return nVertices;
      }

      bool Support::write(std::ostream &stream)
      {
         RECONTOOLS_UINT32 dims = static_cast<RECONTOOLS_UINT32>(this->dimensions());
         RECONTOOLS_UINT32 version = 1;

         stream.write(reinterpret_cast<char const *>(&version), sizeof(version));
         stream.write(reinterpret_cast<char const *>(&dims), sizeof(dims));

         return true;
      }

      bool Support::read(std::istream &stream)
      {
         RECONTOOLS_UINT32 version;
         stream.read(reinterpret_cast<char *>(&version), sizeof(version));

         RECONTOOLS_UINT32 dims;
         stream.read(reinterpret_cast<char *>(&dims), sizeof(dims));

         this->dimensions(dims);

         return true;
      }
   }
}
