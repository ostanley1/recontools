#include <cpplapack/cpplapack.h>
#include <iostream>
#include <iterator>
#include <nlopt.hpp>
#include <vector>
#include <complex>
#include <limits>
#include <numeric>

using namespace std;

int funccount = 0;
double ObjectiveMinimax(unsigned N, const double * weights, double * grad,  void * data) {
	// returns t which we are maximising
	// grad is 1 for t and 0 elsewhere
	funccount++;

	if (grad) {
		fill(grad, grad + N, 0.0);
		*grad = 1.0;
	}

    if (funccount % 10 == 0 || funccount < 5) {
        std::cout << "current weights " << std::flush;
        for (int i=0; i<N; i++) {
            std::cout << *(weights+i) << " " << std::flush;
        }
        std::cout << std::endl;
//        if (grad) {
//            std::cout << std::endl;
//            std::cout << "current grad " << std::flush;
//            for (int i = 0; i < N; i++) {
//                std::cout << *(grad + i) << " " << std::flush;
//            }
//            std::cout << std::endl;
//        }
    }

	return *weights;
}

double PointConstraintMinimax(unsigned N, const double * weights, double * grad, void * data) {
	// returns t-(weights*point)^H*(weights*point) which is held at <= 0
	// grad is 1 for t and 2*(sum(weights*point))*point elsewhere

	// convert dat to indata, cast weights and grad to complex
	auto * indata = reinterpret_cast<complex<double> *> (data);
    auto const * inweights = reinterpret_cast<complex<double> const *> (weights + 1);
    auto * ingrad = reinterpret_cast<complex<double> *> (grad+1);

    // calculate nessecary values for constraints
	double t = *weights;
	complex<double> zero = 0.0 + 0.0i;
    complex<double> coil_value = inner_product(inweights, inweights + (N-1)/2, indata, zero);
//    std::cout << "coil_value: " << coil_value.real() << " " << coil_value.imag() << std::endl;
    coil_value = coil_value * conj(coil_value);

//    std::cout << "coil_value: " << coil_value.real() << " " << coil_value.imag() << std::endl;
//    std::cout << "current weights " << std::flush;
//    for (int i = 0; i < (N-1)/2; i++) {
//        std::cout << *(inweights+i) << " " << std::flush;
//    }
//    std::cout << std::endl;
//    std::cout << "data " << std::flush;
//    for (int i = 0; i < (N-1)/2; i++) {
//        std::cout << *(indata+i) << " " << std::flush;
//    }
//    std::cout << std::endl;

    if (grad) {
        double * iter_grad;
        complex<double> grad_val;
        iter_grad = grad;
        while (iter_grad < grad + N ) {
            if (iter_grad == grad) {
                *iter_grad = 0.0;
                iter_grad++;
            } else {
                grad_val = *ingrad * coil_value;
                *iter_grad = grad_val.real();
                iter_grad++;
                *iter_grad = grad_val.imag();
                iter_grad++;
                ingrad++;
            }
        }
    }

//    if (funccount % 10 ==0 || funccount < 5) {
//        std::cout << "point constraint weights " << std::flush;
//        for (int i=0; i<N; i++) {
//            std::cout << *(weights+i) << " " << std::flush;
//        }
//        std::cout << std::endl;
//
////        if (grad) {
////            std::cout << "current grad " << std::flush;
////            for (int i = 0; i < N; i++) {
////                std::cout << *(grad + i) << " " << std::flush;
////            }
////            std::cout << std::endl;
////            std::cout << std::endl;
////        }
//    }

	return t-coil_value.real();
}

double NormConstraintMinimax(unsigned N, const double * weights, double * grad,  void * data) {
	// returns norm(weights)^2-1
	// grad is 0 for t and 2*weight elsewhere

    double norm = inner_product(weights+1, weights+N, weights+1, -1);

	if (grad) {
	    double * iter_grad;
	    iter_grad = grad;
		while (iter_grad < grad + N) {
            if (iter_grad == grad) {
                *iter_grad = 0.0;
            } else {
                if ((iter_grad - grad) % 2 != 0) {
                    *iter_grad = 2 * *(weights + (iter_grad - grad));
                }
                else {
                    *iter_grad = -2 * *(weights + (iter_grad - grad));
                }

            }
            iter_grad++;
        }
	}

//    if (funccount % 10 ==0 || funccount < 5) {
//        std::cout << "norm " << norm << std::endl;
//        std::cout << "norm constraint weights " << std::flush;
//        for (int i=0; i<N; i++) {
//            std::cout << *(weights+i) << " " << std::flush;
//        }
//        std::cout << std::endl;
//
//        if (grad) {
//            std::cout << "current grad " << std::flush;
//            for (int i = 0; i < N; i++) {
//                std::cout << *(grad + i) << " " << std::flush;
//            }
//            std::cout << std::endl;
//            cout << endl;
//        }
//        else {
//            cout << funccount << endl;
//            cout << "grad undefined" << endl;
//        }
//    }
	return norm;
}

double NegNormConstraintMinimax(unsigned N, const double * weights, double * grad,  void * data) {
    // returns -(norm(weights)^2-1) to be the other half of the inequality
    // grad is 0 for t and 2*weight elsewhere

    double norm = NormConstraintMinimax(N, weights, grad,  data);

    return -norm;
}

/* These two functions provide weights for minimax alignment prior to fitting
This should improve fit robustness by creating a common phase that is singularity free

These functions consist of the following steps:

 RunSVD
    1) Perform SVD on the data in order to initialize weights for minimax algorithm
 External Functions
    2) Create objective and constraint functions for optimizer
    Real bounds for this problem:
        Objective function: f(w) = min(abs(data*w))
        Constraint function: c(w) = norm(w)-1 (aka norm(w)=1)

        Added dummy constraint t as in:
        t is element 0 and 0 is element 1 of weight array
        Creates p constraint functions for points and 1 for norm

        Gradients were found to be:
            Objective: 1, 0...0
            Point Constraint: -1/2 [point - i*point] where point=point at specfic coil val
            Norm Constraint: weight/norm(weights) where weight is a single real or complex double
 RunMinimax
    3) Set up optimization as maximin optimization using LD_SLSQP
        The goal of this is to move the common magnitude across all points away from zero, this remove singularities
    4) Run the optimization
    5) Return the weights from the optimization
*/

int RunSVD(vector<complex<double> > &data, int n_coils, int n_points, vector<double> &weights) {
    // 1) Perform SVD over data array which is coils by points
    // data is the coil data which is coils by points (col-major), we do not wish to overwrite it
    int lwork=-1;
    complex<double> u[n_coils*n_coils], vt[n_points*n_coils], workopt;
    double rwork[5 * n_points], s[n_coils];

    // Query the function for optimal workspace
    cpplapack::gesvd('S', 'N', n_coils, n_points, NULL, n_coils, NULL, u, n_coils, vt, n_points, &workopt, lwork, rwork);

    // Initialize work array
    lwork = (int)workopt.real();
    complex<double> work[lwork];

    // Perform the svd for real
    cpplapack::gesvd('S', 'N', n_coils, n_points, &(data.front()), n_coils, s, u, n_coils, vt, n_points, &work[0], lwork, rwork);

    // 3) Optimization needs all real params
    // Weights are stored in u and need to be put in weights as non-complex data
    memcpy(weights.data(), u, sizeof(*u)*n_coils);
    return 1;
}

int RunMinimax(vector<complex<double> > &data, int n_coils, int n_points, vector<double> &weights) {
	// 4) Run optimization
    *weights.insert(weights.begin(),0); // add extra t variable

    if (funccount % 10 == 0) {
        std::cout << "initial weights " << std::flush;
        for (int i=0; i<n_coils*2+1; i++) {
            std::cout << weights[i] << " " << std::flush;
        }
        std::cout << std::endl;
    }

	// LD_SLSQP is one of a limited # of choice with derivative, inequality and equality constraints allowed
	nlopt::opt opt(nlopt::LD_SLSQP, n_coils * 2 + 1);//LD_SLSQP LN_COBYLA
	opt.set_max_objective(ObjectiveMinimax, NULL);
	opt.add_equality_constraint(NormConstraintMinimax, NULL, 1e-4);
//    opt.add_inequality_constraint(NegNormConstraintMinimax, NULL, 1e-4);
	for (int p=0; p < n_points; p++) {
		opt.add_inequality_constraint(PointConstraintMinimax, reinterpret_cast<void *> (&(data.front()) + p * n_coils), 1e-4);
	}
	opt.set_xtol_rel(1e-8);
    opt.set_initial_step(0.01);
	opt.set_maxeval(10000*n_coils);

	double maxf=-100;
	cout << "starting optimization" << endl;
	int result=opt.optimize(weights, maxf);
    *weights.erase(weights.begin()); // remove bonus variable

	// 5) return weights
	if (result > 0) {
		std::cout << "optimization found maximum at " << std::flush;
		std::copy(weights.begin(), weights.end(), std::ostream_iterator<double>(std::cout, " "));
		std::cout << std::endl;
		std::cout << "Exit code: " << result << " Function count: " << funccount << " Final func value: " << maxf << std::endl;
        funccount = 0;
		return result;
	}
	else {
        std::cout << "optimization failed with error:  " << result << std::flush;
        std::copy(weights.begin(), weights.end(), std::ostream_iterator<double>(std::cout, " "));
        std::cout << std::endl;
        std::cout << "Exit code: " << result << " Function count: " << funccount << std::endl;
        funccount = 0;
        return result;
	}

}

