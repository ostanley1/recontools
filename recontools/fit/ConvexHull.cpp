//
// Created by Martyn Klassen on 2019-12-06.
//

#include <cstring>
#include <vector>
#include <algorithm>
#include <stdexcept>
#include <set>
#include <limits>
#include "ConvexHull.hpp"


namespace recontools
{
   namespace fit
   {
      void get_closest(const P3 &point, const Face &facet, P3 &location);

      struct Separation
      {
         P3::value_type distance;
         Face *face;

         friend bool operator<(const Separation &a, const Separation &b)
         {
            return a.distance < b.distance;
         }
      };

      struct Snork
      {
         int id;
         int a, b;

         Snork()
               : id(-1), a(0), b(0)
         {};

         Snork(int i, int r, int x)
               : id(i), a(r), b(x)
         {};

         Snork(const Snork &p)
               : id(p.id), a(p.a), b(p.b)
         {};

         Snork &operator=(const Snork &p)
         {
            id = p.id;
            a = p.a;
            b = p.b;

            return *this;
         };

         friend bool operator<(const Snork &a, const Snork &b)
         {
            if (a.a == b.a)
            {
               return a.b < b.b;
            }
            return a.a < b.a;
         }
      };


      // cross product relative sign test.
      int cross_test(std::vector<R3> &pts, int A, int B, int C, int X, P3 &e)
      {
         P3 AB(pts[B] - pts[A]);
         e = cross(AB, pts[X] - pts[A]);

         //  look at sign of (ab x ac).(ab x ax)
         P3::value_type signValue = e * cross(AB, pts[C] - pts[A]);

         if (signValue > 0.0) return (1);
         if (signValue < 0.0) return (-1);

         return 0;
      }

      void setup_plates(Tri &up, Tri &down, const int &size, const int &id, const int &A, const int &B, const P3 &e)
      {
         up.keep = 2;
         up.id = size;
         up.a = id;
         up.b = A;
         up.c = B;

         up.e = e;
         up.ab = -1;
         up.ac = -1;

         down.keep = 2;
         down.id = size + 1;
         down.a = id;
         down.b = A;
         down.c = B;

         down.ab = -1;
         down.ac = -1;
         down.e = -1.0 * e;
      }

      void add_coplanar(std::vector<R3> &pts, std::vector<Tri> &hull, int id)
      {

         int numHulls = static_cast<int>(hull.size());
         P3 e;

         for (int k = 0; k < numHulls; k++)
         {
            //find visible edges. from external edges.

            if (hull[k].c == hull[hull[k].ab].c)
            { // ->  ab is an external edge.
               // test this edge for visibility from new point pts[id].
               int A = hull[k].a;
               int B = hull[k].b;
               int C = hull[k].c;

               int zot = cross_test(pts, A, B, C, id, e);

               if (zot < 0)
               { // visible edge facet, create 2 new hull plates.
                  Tri up, down;
                  setup_plates(up, down, static_cast<int>(hull.size()), id, A, B, e);

                  if (hull[k].e * e > 0)
                  {
                     up.bc = k;
                     down.bc = hull[k].ab;

                     hull[k].ab = up.id;
                     hull[down.bc].ab = down.id;
                  }
                  else
                  {
                     down.bc = k;
                     up.bc = hull[k].ab;

                     hull[k].ab = down.id;
                     hull[up.bc].ab = up.id;
                  }

                  hull.push_back(up);
                  hull.push_back(down);
               }
            }


            if (hull[k].a == hull[hull[k].bc].a)
            {   // bc is an external edge.
               // test this edge for visibility from new point pts[id].
               int A = hull[k].b;
               int B = hull[k].c;
               int C = hull[k].a;

               int zot = cross_test(pts, A, B, C, id, e);

               if (zot < 0)
               { // visible edge facet, create 2 new hull plates.
                  Tri up, down;
                  setup_plates(up, down, static_cast<int>(hull.size()), id, A, B, e);

                  if (hull[k].e * e > 0)
                  {
                     up.bc = k;
                     down.bc = hull[k].bc;

                     hull[k].bc = up.id;
                     hull[down.bc].bc = down.id;
                  }
                  else
                  {
                     down.bc = k;
                     up.bc = hull[k].bc;

                     hull[k].bc = down.id;
                     hull[up.bc].bc = up.id;
                  }

                  hull.push_back(up);
                  hull.push_back(down);
               }
            }


            if (hull[k].b == hull[hull[k].ac].b)
            {   // ac is an external edge.
               // test this edge for visibility from new point pts[id].
               int A = hull[k].a;
               int B = hull[k].c;
               int C = hull[k].b;

               int zot = cross_test(pts, A, B, C, id, e);

               if (zot < 0)
               { // visible edge facet, create 2 new hull plates.
                  Tri up, down;
                  setup_plates(up, down, static_cast<int>(hull.size()), id, A, B, e);

                  if (hull[k].e * e > 0)
                  {
                     up.bc = k;
                     down.bc = hull[k].ac;

                     hull[k].ac = up.id;
                     hull[down.bc].ac = down.id;
                  }
                  else
                  {
                     down.bc = k;
                     up.bc = hull[k].ac;

                     hull[k].ac = down.id;
                     hull[up.bc].ac = up.id;
                  }

                  hull.push_back(up);
                  hull.push_back(down);
               }
            }
         }


         // fix up the non assigned hull adjacencies (correctly).
         int numN = static_cast<int>(hull.size());
         std::vector<Snork> norts;
         Snork snort;
         for (int q = numN - 1; q >= numHulls; q--)
         {
            if (hull[q].keep > 1)
            {
               snort.id = q;
               snort.a = hull[q].b;
               snort.b = 1;
               norts.push_back(snort);

               snort.a = hull[q].c;
               snort.b = 0;
               norts.push_back(snort);

               hull[q].keep = 1;
            }
         }

         std::sort(norts.begin(), norts.end());
         int nums = static_cast<int>(norts.size());
         Snork snor;
         snor.id = -1;
         snor.a = -1;
         snor.b = -1;
         norts.push_back(snor);
         snor.id = -2;
         snor.a = -2;
         snor.b = -2;
         norts.push_back(snor);

         if (nums >= 2)
         {
            for (int s = 0; s < nums - 1; s++)
            {
               if (norts[s].a == norts[s + 1].a)
               {
                  // link triangle sides.
                  if (norts[s].a != norts[s + 2].a)
                  { // edge of figure case
                     if (norts[s].b == 1)
                     {
                        hull[norts[s].id].ab = norts[s + 1].id;
                     }
                     else
                     {
                        hull[norts[s].id].ac = norts[s + 1].id;
                     }

                     if (norts[s + 1].b == 1)
                     {
                        hull[norts[s + 1].id].ab = norts[s].id;
                     }
                     else
                     {
                        hull[norts[s + 1].id].ac = norts[s].id;
                     }
                     s++;
                  }
                  else
                  { // internal figure boundary 4 junction case.
                     int s1 = s + 1, s2 = s + 2, s3 = s + 3;

                     P3 &hull0 = hull[norts[s].id].e;

                     // check normal directions of id and id1..3
                     double barf = hull0 * hull[norts[s1].id].e;
                     if (barf <= 0)
                     {
                        barf = hull0 * hull[norts[s2].id].e;
                        if (barf > 0)
                        {
                           std::swap(s2, s1);
                        }
                        else
                        {
                           barf = hull0 * hull[norts[s3].id].e;
                           if (barf > 0)
                           {
                              std::swap(s3, s1);
                           }
                        }
                     }

                     if (norts[s].b == 1)
                     {
                        hull[norts[s].id].ab = norts[s1].id;
                     }
                     else
                     {
                        hull[norts[s].id].ac = norts[s1].id;
                     }

                     if (norts[s1].b == 1)
                     {
                        hull[norts[s1].id].ab = norts[s].id;
                     }
                     else
                     {
                        hull[norts[s1].id].ac = norts[s].id;
                     }


                     // use s2 and s3
                     if (norts[s2].b == 1)
                     {
                        hull[norts[s2].id].ab = norts[s3].id;
                     }
                     else
                     {
                        hull[norts[s2].id].ac = norts[s3].id;
                     }

                     if (norts[s3].b == 1)
                     {
                        hull[norts[s3].id].ab = norts[s2].id;
                     }
                     else
                     {
                        hull[norts[s3].id].ac = norts[s2].id;
                     }

                     s += 3;
                  }
               }
            }
         }

         return;
      }

      int init_hull3D(std::vector<R3> &pts, std::vector<Tri> &hull)
      {
         // Inputs are required to be in sorted order

         int numPoints = (int) pts.size();
         Tri T1(0, 1, 2);

         P3 &P0 = pts[T1.a];
         P3 &P1 = pts[T1.b];
         P3 &P2 = pts[T1.c];

         P3 d01 = P1 - P0;
         P3 d02 = P2 - P0;

         // check for colinearity
         P3 e = cross(d01, d02);
         int i = 3;
         while ((e == 0.0) && (i < numPoints))
         {
            T1.b = T1.c;
            T1.c = i;

            P1 = P2;
            P2 = pts[T1.c];
            d01 = d02;
            d02 = P2 - P0;

            e = cross(d01, d02);

            i++;
         }

         // All points are colinear
         if (i >= numPoints)
         {
            throw std::runtime_error("All points are colinear");
         }

         P3 M = P0 + P1 + P2;

         T1.id = 0;
         T1.e = e;

         T1.ab = 1;   // adjacent facet id number
         T1.ac = 1;
         T1.bc = 1;

         hull.push_back(T1);

         T1.id = 1;
         T1.e = -1.0 * e;

         T1.ab = 0;
         T1.ac = 0;
         T1.bc = 0;

         hull.push_back(T1);
         std::vector<int> xlist;
         Tri newTriangle;

         P3 m;
         for (int p = i; p < numPoints; p++)
         { // add points until a non coplanar set of points is achieved.
            R3 &pt = pts[p];

            M += pt;
            m = M * (1.0 / (p + 1));

            // find the first visible plane.
            int numHulls = static_cast<int>(hull.size());
            int visibleHull = -1;
            xlist.clear();

            for (int h = numHulls - 1; h >= 0; h--)
            {
               Tri &t = hull[h];

               if ((pt - pts[t.a]) * t.e > 0)
               {
                  visibleHull = h;
                  hull[h].keep = 0;
                  xlist.push_back(visibleHull);
                  break;
               }
            }
            if (visibleHull < 0)
            {
               add_coplanar(pts, hull, p);
            }
            if (visibleHull >= 0)
            {
               // new triangular facets are formed from neighbouring invisible planes.
               numHulls = static_cast<int>(hull.size());
               int numXList = static_cast<int>(xlist.size());
               for (int x = 0; x < numXList; x++)
               {
                  int xid = xlist[x];
                  int ab = hull[xid].ab;      // facet adjacent to line ab
                  Tri &tAB = hull[ab];

                  if ((pt - pts[tAB.a]) * tAB.e > 0)
                  { // add to xlist.
                     if (hull[ab].keep == 1)
                     {
                        hull[ab].keep = 0;
                        xlist.push_back(ab);
                        numXList++;
                     }
                  }
                  else
                  { // spawn a new triangle.
                     newTriangle.id = static_cast<int>(hull.size());
                     newTriangle.keep = 2;
                     newTriangle.a = p;
                     newTriangle.b = hull[xid].a;
                     newTriangle.c = hull[xid].b;

                     newTriangle.ab = -1;
                     newTriangle.ac = -1;
                     newTriangle.bc = ab;

                     // make normal vector.
                     e = cross(pts[newTriangle.a] - pts[newTriangle.b],
                               pts[newTriangle.a] - pts[newTriangle.c]);

                     // make it point outwards.
                     if ((m - pt) * e > 0)
                     {
                        newTriangle.e = -1.0 * e;
                     }
                     else
                     {
                        newTriangle.e = e;
                     }

                     // update the touching triangle tAB
                     int A = hull[xid].a, B = hull[xid].b;
                     if ((tAB.a == A && tAB.b == B) || (tAB.a == B && tAB.b == A))
                     {
                        tAB.ab = static_cast<int>(hull.size());
                     }
                     else if ((tAB.a == A && tAB.c == B) || (tAB.a == B && tAB.c == A))
                     {
                        tAB.ac = static_cast<int>(hull.size());
                     }
                     else if ((tAB.b == A && tAB.c == B) || (tAB.b == B && tAB.c == A))
                     {
                        tAB.bc = static_cast<int>(hull.size());
                     }
                     else
                     {
                        throw std::runtime_error("hull is numerically unstable");
                     }

                     hull.push_back(newTriangle);
                  }

                  // second side of the struck out triangle

                  int ac = hull[xid].ac;     // facet adjacent to line ac
                  Tri &tAC = hull[ac];

                  // point on next triangle
                  if ((pt - pts[tAC.a]) * tAC.e > 0)
                  { // add to xlist.
                     if (hull[ac].keep == 1)
                     {
                        hull[ac].keep = 0;
                        xlist.push_back(ac);
                        numXList++;
                     }
                  }
                  else
                  { // spawn a new triangle.
                     newTriangle.id = static_cast<int>(hull.size());
                     newTriangle.keep = 2;
                     newTriangle.a = p;
                     newTriangle.b = hull[xid].a;
                     newTriangle.c = hull[xid].c;

                     newTriangle.ab = -1;
                     newTriangle.ac = -1;
                     newTriangle.bc = ac;

                     // make normal vector.
                     e = cross(pts[newTriangle.a] - pts[newTriangle.b],
                               pts[newTriangle.a] - pts[newTriangle.c]);

                     // points from new facet towards [mr,mc,mz]
                     // make it point outwards.
                     if ((m - pt) * e > 0)
                     {
                        newTriangle.e = -1.0 * e;
                     }
                     else
                     {
                        newTriangle.e = e;
                     }
                     // update the touching triangle tAC
                     int A = hull[xid].a, C = hull[xid].c;
                     if ((tAC.a == A && tAC.b == C) || (tAC.a == C && tAC.b == A))
                     {
                        tAC.ab = static_cast<int>(hull.size());
                     }
                     else if ((tAC.a == A && tAC.c == C) || (tAC.a == C && tAC.c == A))
                     {
                        tAC.ac = static_cast<int>(hull.size());
                     }
                     else if ((tAC.b == A && tAC.c == C) || (tAC.b == C && tAC.c == A))
                     {
                        tAC.bc = static_cast<int>(hull.size());
                     }
                     else
                     {
                        throw std::runtime_error("Numerical instability");
                     }

                     hull.push_back(newTriangle);
                  }

                  // third side of the struck out triangle

                  int bc = hull[xid].bc;     // facet adjacent to line ac
                  Tri &tBC = hull[bc];

                  if ((pt - pts[tBC.a]) * tBC.e > 0)
                  { // add to xlist.
                     if (hull[bc].keep == 1)
                     {
                        hull[bc].keep = 0;
                        xlist.push_back(bc);
                        numXList++;
                     }
                  }
                  else
                  { // spawn a new triangle.
                     newTriangle.id = static_cast<int>(hull.size());
                     newTriangle.keep = 2;
                     newTriangle.a = p;
                     newTriangle.b = hull[xid].b;
                     newTriangle.c = hull[xid].c;

                     newTriangle.ab = -1;
                     newTriangle.ac = -1;
                     newTriangle.bc = bc;

                     // make normal vector.
                     e = cross(pts[newTriangle.a] - pts[newTriangle.b],
                               pts[newTriangle.a] - pts[newTriangle.c]);

                     // points from new facet towards [mr,mc,mz]
                     // make it point outwards.
                     if ((m - pt) * e > 0)
                     {
                        newTriangle.e = -1.0 * e;
                     }
                     else
                     {
                        newTriangle.e = e;
                     }

                     // update the touching triangle tBC
                     int B = hull[xid].b, C = hull[xid].c;
                     if ((tBC.a == B && tBC.b == C) || (tBC.a == C && tBC.b == B))
                     {
                        tBC.ab = static_cast<int>(hull.size());
                     }
                     else if ((tBC.a == B && tBC.c == C) || (tBC.a == C && tBC.c == B))
                     {
                        tBC.ac = static_cast<int>(hull.size());
                     }
                     else if ((tBC.b == B && tBC.c == C) || (tBC.b == C && tBC.c == B))
                     {
                        tBC.bc = static_cast<int>(hull.size());
                     }
                     else
                     {
                        throw std::runtime_error("Numerical instability");
                     }

                     hull.push_back(newTriangle);
                  }
               }

               // patch up the new triangles in hull.
               int numN = static_cast<int>(hull.size());
               std::vector<Snork> norts;
               Snork snort;
               for (int q = numN - 1; q >= numHulls; q--)
               {
                  if (hull[q].keep > 1)
                  {
                     snort.id = q;
                     snort.a = hull[q].b;
                     snort.b = 1;
                     norts.push_back(snort);

                     snort.a = hull[q].c;
                     snort.b = 0;
                     norts.push_back(snort);

                     hull[q].keep = 1;
                  }
               }

               std::sort(norts.begin(), norts.end());
               int nums = static_cast<int>(norts.size());

               if (nums >= 2)
               {
                  for (int s = 0; s < nums - 1; s++)
                  {
                     if (norts[s].a == norts[s + 1].a)
                     {
                        // link triangle sides.
                        if (norts[s].b == 1)
                        {
                           hull[norts[s].id].ab = norts[s + 1].id;
                        }
                        else
                        {
                           hull[norts[s].id].ac = norts[s + 1].id;
                        }

                        if (norts[s + 1].b == 1)
                        {
                           hull[norts[s + 1].id].ab = norts[s].id;
                        }
                        else
                        {
                           hull[norts[s + 1].id].ac = norts[s].id;
                        }
                     }
                  }
               }
            }
         }

         return (0);
      }

      void ConvexHull::setPosition(value_type *offset)
      {
         P3 point;
         calculatePosition(reinterpret_cast<P3::value_type *> (&point), offset);
         m_pWork->push_back(R3(point));
      }

      void ConvexHull::setupWork(size_t numPoints)
      {
         if (!m_pWork)
            throw std::runtime_error("Work not assigned");

         m_pWork->reserve(numPoints);
      }


      void ConvexHull::computePerimeter(const mask_type &mask, const int &nDims, const int *dims, const value_type *center, const value_type *voxel)
      {
         if (nDims != 3)
            throw std::runtime_error("Convex hull only support 3D spaces");

         work_type points;
         m_pWork = &points;

         configure(mask, nDims, dims, center, voxel);

         // There will always be at least 8 point on the hull
         // Data is already sorted
      #if 0
         int numPoints = (int) points.size();
            if( numPoints <= 4 ){
            throw std::runtime_error("Less than 4 points for hull");
            }
            std::sort( points.begin(), points.end() );
      #endif

         std::vector<Tri> hull;
         init_hull3D(points, hull);

      #if 0
         // just pick out the hull triangles and renumber.
            int numHulls = static_cast<int>(hull.size());
            std::vector<int> taken(numHulls, -1);

            int cnt = 0;
            int t;
            for(t=0; t<numHulls; t++){      // create an index from old tri-id to new tri-id.
            if( hull[t].keep > 0 ){     // point index remains unchanged.
            taken[t] = cnt++;
            }
            }

            for(t=0; t<numHulls; t++){      // create an index from old tri-id to new tri-id.
            if( hull[t].keep > 0 ){     // point index remains unchanged.
            Tri T = hull[t];
            T.id = taken[t];
            if( taken[T.ab] < 0 ) throw std::runtime_error("Broken hull");
            T.ab = taken[T.ab];

            if( taken[T.bc] < 0 ) throw std::runtime_error("Broken hull");
            T.bc = taken[T.bc];

            if( taken[T.ac] < 0 ) throw std::runtime_error("Broken hull");
            T.ac = taken[T.ac];

            m_vHull.push_back(T);
            }
            }
      #endif

         // Need to store the vertices and the facets
         size_t numHulls = hull.size();
         size_t numPoints = points.size();
         size_t i;

         // Determine which points are vertices of the convex hull
         std::vector<int> map(numPoints, -1);
         size_t numFacets(0);
         int cnt(0);
         for (i = 0; i < numHulls; i++)
         {
            Tri &T = hull[i];
            if (T.keep > 0)
            {
               // Map the used  points to new indices of just vertices
               if (map[T.a] < 0) map[T.a] = cnt++;
               if (map[T.b] < 0) map[T.b] = cnt++;
               if (map[T.c] < 0) map[T.c] = cnt++;

               // Update the indices to new vertices
               T.a = map[T.a];
               T.b = map[T.b];
               T.c = map[T.c];

               // Count the total number of facets
               numFacets++;
            }
         }

         P3 pntCenter;
         m_vVertices.resize(static_cast<size_t>(cnt));
         for (i = 0; i < numPoints; i++)
         {
            // Check if the point is being used, i.e. is mapped
            if (map[i] >= 0)
            {
               // Copy the point to the vertices vector
               m_vVertices[map[i]] = points[i];

               // Sum all the vertices to get center
               pntCenter += points[i];
            }
         }
         pntCenter *= (1.0 / static_cast<double>(numPoints));

         // Store the facets
         m_vFacets.reserve(numFacets);
         for (i = 0; i < numHulls; i++)
         {
            Tri &T = hull[i];
            if (T.keep > 0)
            {
               m_vFacets.push_back(Facet(T, m_vVertices));
            }
         }

         precompute();
      }

      void ConvexHull::precompute()
      {
         m_vFaces.clear();
         m_vFaces.reserve(m_vFacets.size());
         facets_type::const_iterator iterFacets(m_vFacets.begin());
         facets_type::const_iterator endFacets(m_vFacets.end());
         while (iterFacets < endFacets)
         {
            m_vFaces.push_back(Face(m_vVertices[iterFacets->a], m_vVertices[iterFacets->b], m_vVertices[iterFacets->c]));
            iterFacets++;
         }
      }

      bool ConvexHull::interior(const P3 &pos)
      {
         bool returnValue(true);
         std::vector<Face>::iterator iterFaces(m_vFaces.begin());
         std::vector<Face>::const_iterator iterEnd(m_vFaces.end());
         while (iterFaces < iterEnd && returnValue)
         {
            returnValue &= iterFaces->inside(pos);
            iterFaces++;
         }
         return returnValue;
      }


      bool ConvexHull::closest(P3 &point)
      {
         // Inside, so no change to get closest point
         if (interior(point)) return true;

         P3 location;
         P3 minLocation;
         P3::value_type distance;

         double minDistance(std::numeric_limits<double>::max());

         // Sort by distance between point and circumcenter
         std::set<Separation> separation;
         size_t t;
         P3 d;
         Separation s;
         for (t = 0; t < m_vFaces.size(); t++)
         {
            s.face = &(m_vFaces[t]);

            // Get the distance from the point to
            d = s.face->position - point;

            // Store the distance from the point to the circumcircle
            // This is the minimum separation of the face and the point
            // Negative values means the point is within the circumcircle
            s.distance = d.len() - s.face->radius;

            // Set is stored from lowest to highest
            separation.insert(s);
         }

         std::set<Separation>::iterator iterDistance(separation.begin());
         std::set<Separation>::iterator endDistance(separation.end());
         // If the distance from the circumcircle is more than the minDistance
         // then this and all subsequent facets are further from the point
         while ((iterDistance != endDistance) && (iterDistance->distance < minDistance))
         {
            // Find closest point to face
            get_closest(point, *(iterDistance->face), location);

            distance = (location - point).len();
            if (distance < minDistance)
            {
               minDistance = distance;
               minLocation = location;
            }

            iterDistance++;
         }

         // Assign closet location on convex hull to pos
         point = minLocation;
         return false;
      }


      void get_closest(const P3 &point, const Face &facet, P3 &location)
      {
         // Check if outside edge AB
         if (facet.NAB * point < 0)
         {
            // Check if outside edge BC
            if (facet.NBC * point < 0)
            {
               // Closest point is B
               location = facet.B;
               return;
            }

            // Check if outside edge CA
            if (facet.NCA * point < 0)
            {
               // Closest point is A
               location = facet.A;
               return;
            }

            // Closest point is on AB
            // Find the point on the line that is closest
            location = facet.A + (facet.AB * (point - facet.A)) * facet.AB;
            return;
         }

         // Must be inside edge AB

         // Check if outside edge BC
         if (facet.NBC * point < 0)
         {
            // Check if outside edge CA
            if (facet.NCA * point < 0)
            {
               // Closest point is C
               location = facet.C;
               return;
            }

            // Closest point is on BC
            location = facet.B + (facet.BC * (point - facet.B)) * facet.BC;
            return;
         }

         // Must be inside edge AB and edge BC

         // Check is outside edge CA
         if (facet.NCA * point < 0)
         {
            // Closest point is on CA
            location = facet.C + (facet.CA * (point - facet.C)) * facet.CA;
            return;
         }

         // Closest point is on ABC
         // Find projection onto the plane
         location = point - (facet.normal * (point - facet.position)) * facet.normal;
      }

      bool ConvexHull::write(std::ostream &stream)
      {
         if (!Support::write(stream))
            return false;

         RECONTOOLS_UINT32 version = 2;
         stream.write(reinterpret_cast<const char *>(&version), sizeof(version));

         RECONTOOLS_UINT32 numVertices = static_cast<int>(m_vVertices.size());
         stream.write(reinterpret_cast<const char *>(&numVertices), sizeof(numVertices));
         stream.write(reinterpret_cast<const char *>(RECONTOOLS_VECTOR_POINTER(m_vVertices)), sizeof(m_vVertices.front()) * m_vVertices.size() );

         RECONTOOLS_UINT32 numFacets = static_cast<int>(m_vFacets.size());
         stream.write(reinterpret_cast<const char *>(&numFacets), sizeof(numFacets));
         stream.write(reinterpret_cast<const char *>(RECONTOOLS_VECTOR_POINTER(m_vFacets)), sizeof(m_vFacets.front()) * m_vFacets.size() );

         return true;
      }

      bool ConvexHull::read(std::istream &stream)
      {
         if (!Support::read(stream))
            return false;

         RECONTOOLS_UINT32 version;
         RECONTOOLS_UINT32 numVertices;
         RECONTOOLS_UINT32 numFacets;

         stream.read(reinterpret_cast<char *>(&version), sizeof(version));

         stream.read(reinterpret_cast<char *>(&numVertices), sizeof(numVertices));
         P3 vertex;
         for (RECONTOOLS_UINT32 i=0; i<numVertices; i++) {
            stream.read(reinterpret_cast<char *>(&vertex), sizeof(vertex));
            m_vVertices.push_back(vertex);
         }

         stream.read(reinterpret_cast<char *>(&numFacets), sizeof(numFacets));
         int a,b,c;
         for (RECONTOOLS_UINT32 i=0; i<numFacets; i++) {
            stream.read(reinterpret_cast<char *>(&a), sizeof(a));
            stream.read(reinterpret_cast<char *>(&b), sizeof(b));
            stream.read(reinterpret_cast<char *>(&c), sizeof(c));
            m_vFacets.push_back(Facet(a, b, c));
         }

         precompute();

         return true;
      }
   }
}
