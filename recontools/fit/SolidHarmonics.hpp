//
// Created by Martyn Klassen on 2019-12-09.
//

#ifndef RECONTOOLS_SOLIDHARMONICS_HPP
#define RECONTOOLS_SOLIDHARMONICS_HPP

#include <cstddef>
#include <cassert>
#include <vector>
#include <cfloat>
#include <limits>
#include <complex>

#include "recontools/fit/Support.hpp"

namespace recontools
{
   namespace fit
   {

      template<typename T>
      static inline std::complex<T> zunity(const std::complex<T> &w)
      {
         T U, V, Q, S;

         U = fabs(w.real());
         V = fabs(w.imag());
         S = U + V;
         if (S == 0.0) return 1.0;
         if (U > V)
         {
            Q = V / U;
            return w / (U * sqrt(1.0 + Q * Q));
         }
         Q = U / V;
         return w / (V * sqrt(1.0 + Q * Q));
      }

      template<typename T>
      T length(T &a, T &b, T &c)
      {
         T u, v, x, q, s;

         u = fabs(a);
         v = fabs(b);
         x = fabs(c);

         if (s == 0.0) return 0.0;

         if (u > v)
         {
            if (u > x)
            {
               // max u
               q = v / u;
               s = q * q;
               q = x / u;
               s += q * q;
               return u * sqrt(1.0 + s);
            }
         }
         else if (v > x)
         {
            // max v
            q = u / v;
            s = q * q;
            q = x / v;
            s += q * q;
            return v * sqrt(1.0 + s);
         }
         // max x
         q = u / x;
         s = q * q;
         q = v / x;
         s += q * q;
         return x * sqrt(1.0 + s);
      }

      template<typename T>
      T length(T &a, T &b)
      {
         T u, v, q;
         u = fabs(a);
         v = fabs(b);
         q = u + v;
         if (q == 0.0) return 0.0;
         if (u > v)
         {
            q = v / u;
            return u * sqrt(1.0 + q * q);
         }
         q = u / v;
         return v * sqrt(1.0 + q * q);
      }


      template<typename T, typename R>
      class Harmonic
      {
      public:
         typedef T value_type;
         typedef R result_type;
         typedef std::vector<result_type> data_type;

         explicit Harmonic(size_t order = 5)
               : m_order(order)
               , m_radius(order + 1)
         {
            assert(m_order > 1);
         }

         virtual ~Harmonic()
         {}

         virtual void compute(P3 const &position, data_type &values);

         virtual void compute(P3 const &position, result_type *values);

         virtual void compute(T *position, result_type *values);

         virtual size_t terms() const
         {
            size_t terms = m_order + 1;
            return terms * terms;
         }

         void order(size_t order)
         {
            m_order = order;
         }

         size_t order() const
         {
            return m_order;
         }

      protected:
         virtual result_type *radial(T radius) = 0;

         size_t m_order;
         std::vector<result_type> m_radius;
      };

      template<typename T, typename R>
      void Harmonic<T, R>::compute(P3 const &pos, data_type &values)
      {
         compute(pos, &(values.front()));
      }

      template<typename T, typename R>
      void Harmonic<T, R>::compute(P3 const &pos, result_type *values)
      {
         T position[3];
         position[0] = pos.r;
         position[1] = pos.c;
         position[2] = pos.z;

         compute(position, values);
      }

      template<typename T, typename R>
      void Harmonic<T, R>::compute(T *position, result_type *values)
      {
         double sumOfMx2, fact, pll, pmm, pmmPlus1, pmm_next;
         size_t m, l;

         // Computes the spherical harmonics
         // A(l,0) = r^l * sqrt( (2*l+1) / (4 * PI) ) * P_l^0(cos(theta))
         // A(l,m) = (-1)^m * r^l * sqrt( (2*l+1) / (4 * PI) * (l-m)! / (l+m)!) * P_l^m(cos(theta)) * cos(m*phi)
         // B(l,m) = (-1)^m * r^l * sqrt( (2*l+1) / (4 * PI) * (l-m)! / (l+m)!) * P_l^m(cos(theta)) * sin(m*phi)

         // Fills in all P_l^m value in order (l,m)
         // A(0,0) A(1,0) A(2,0) ... A(order, 0)
         // A(1,1) B(1,1) A(2,1) B(2,1) ... A(order,1) B(order,1)
         // ...
         // A(order,order) B(order,order)

         // Calculate the spherical coordinates from cartesian
         // x = cos(theta)
         // phi = phi
         // r = r (length)

         // Compute the distance
         double r_polar = length(position[0], position[1]);
         fact = length(r_polar, position[2]);

         double x;
         // Regardless of rounding errors
         // r_l[1] must be >= 0.0 because sqrt(+x) >= 0.0
         // r_l[1] must be <= position[2]
         // Just need to check not equal to zero
         if (fact >= 100.0 * std::numeric_limits<double>::epsilon())
            x = position[2] / fact;
         else
         {
            // Radius is 0, trivial case
            memset(values, 0, (m_order + 1) * (m_order + 1) * sizeof(*values));
            *values = sqrt(0.25 / M_PI);
            return;
         }

         // Switch from mm to m
         R *radial = this->radial(fact / 1000.0);

         // atan2, sin, and cos are expensive
         // It might be more efficient to use
         // cos(m*phi) = T_n(cos(phi))
         // sin(m*phi) = sqrt(1.0 - cos^2(m *phi))
         // T_0(x) = 1
         // T_1(x) = x
         // T_n+1(x) = 2 * x * T_n(x) - T_n-1(x)

         // U_n(cos(phi)) = sin((n+1) * phi) / sin(phi)
         // U_0(x) = 1
         // U_1(x) = 2 * x
         // U_n+1(x) = 2 * x * U_n(x) - U_n-1(x)
         double cos_phi = 1.0;
         double sin_phi = 0.0;
         if (r_polar >= 100.0 * std::numeric_limits<double>::epsilon())
         {
            cos_phi = position[0] / r_polar;
            sin_phi = position[1] / r_polar;
         }

         //double phi = atan2(position[1], position[0]);

         // Chebychev polynomials on cos(phi)
         double T_n, T_n_1, T_n_2;
         double U_n, U_n_1, U_n_2;

         T_n_1 = 1.0;
         U_n_1 = 1.0;
         T_n = cos_phi;
         U_n = 2.0 * cos_phi;

         double cos_m_phi, sin_m_phi;

         // Initialize values used to compute associated legendre polynomials
         // of cos(theta)
         fact = pmm = pmm_next = 1.0;
         sumOfMx2 = sqrt((1.0 - x) * (1.0 + x));

         // Start the computation with the m = 0 terms
         // No scaling in th A(l,0) term
         // r^0 = 1.0 / sqrt(4 * PI)
         radial[0] *= sqrt(0.25 / M_PI);
         *values++ = pmm * radial[0];
         pmmPlus1 = x * pmm;

         // Build in the sqrt((2*l+1)/(4*PI)) scaling into r_l
         // This is l=1 term
         radial[1] *= sqrt(0.75 / M_PI);

         // r^1
         *values++ = pmmPlus1 * radial[1];

         for (l = 2; l <= m_order; l++)
         {
            // Iterative build with m == 0
            pll = (x * static_cast<double>(2ul * l - 1ul) * pmmPlus1 - static_cast<double>(l - 1ul) * pmm) / static_cast<double>(l);
            pmm = pmmPlus1;
            pmmPlus1 = pll;

            // Build in the sqrt((2*l+1)/(4*pi)) scaling into r_l
            radial[l] *= sqrt(static_cast<double>(2ul * l + 1ul) / (4.0 * M_PI));

            *values++ = pll * radial[l];
         }

         // Now build up the A and B terms for m > 0
         double norm_arg;
         result_type value;
         double norm_denom = 1.0;
         double sign(-1.0);

         for (m = 1; m < m_order; m++)
         {
            l = m;

            // Calculate associated legendre polynomial for m and l=m
            pmm_next *= fact * sumOfMx2;
            fact += 2.0;
            pmm = pmm_next;

            // Calculate the normalization factor for m = l
            // sqrt((l-m)!/(l+m)!) = sqrt(1/(2*l)!)
            norm_denom *= static_cast<double>((4ul * l - 2ul) * l); // (2*l) * (2*l - 1)
            norm_arg = 1.0 / norm_denom;
            value = sign * sqrt(norm_arg) * pmm * radial[l];

            cos_m_phi = T_n;
            sin_m_phi = U_n_1 * sin_phi;
            //cos_m_phi = cos(m*phi);
            //sin_m_phi = sin(m*phi);

            // Assign solid harmonics
            *values++ = value * cos_m_phi;
            *values++ = value * sin_m_phi;

            // Calculate associated legendre polynomial for m and l=m+1
            l++;
            pmmPlus1 = x * static_cast<double>(2ul * m + 1ul) * pmm;

            // Calculate the normalization factor
            // (l-m) / (l+m) = 1 / (l+m)
            norm_arg *= 1.0 / static_cast<double>(l + m);
            value = sign * sqrt(norm_arg) * pmmPlus1 * radial[l];

            // Assign solid harmonics
            *values++ = value * cos_m_phi;
            *values++ = value * sin_m_phi;

            for (l++; l <= m_order; l++)
            {
               // Iterative build of associated legendre polynomial for m and l=m+2, m+3, ...
               pll = (x * static_cast<double>(2ul * l - 1ul) * pmmPlus1 - static_cast<double>(l + m - 1ul) * pmm) / static_cast<double>(l - m);
               pmm = pmmPlus1;
               pmmPlus1 = pll;

               // Calculate normalization factor
               norm_arg *= static_cast<double>(l - m) / static_cast<double>(l + m);
               value = sign * sqrt(norm_arg) * pll * radial[l];

               // Assign solid harmonics
               *values++ = value * cos_m_phi;
               *values++ = value * sin_m_phi;
            }

            T_n_2 = T_n_1;
            T_n_1 = T_n;
            T_n = 2.0 * cos_phi * T_n_1 - T_n_2;

            U_n_2 = U_n_1;
            U_n_1 = U_n;
            U_n = 2.0 * cos_phi * U_n_1 - U_n_2;
            sign *= -1.0;
         }

         // Calculate associated legendre polynomial for m=l=order
         //m = l = order;
         pmm_next *= fact * sumOfMx2;

         // Calculate normalization factor
         norm_denom *= static_cast<double>((4ul * m_order - 2ul) * m_order);
         value = sign * sqrt((1.0 / norm_denom)) * pmm_next * radial[m_order];

         cos_m_phi = T_n;
         sin_m_phi = U_n_1 * sin_phi;
         //cos_m_phi = cos(m*phi);
         //sin_m_phi = sin(m*phi);

         // Assign solid harmonics
         *values++ = value * cos_m_phi;
         *values = value * sin_m_phi;
      }


      template<typename T, typename R = T>
      class SolidHarmonic : public Harmonic<T, R>
      {
      private:
         typedef Harmonic<T, R> parent_type;

      public:
         typedef typename parent_type::result_type result_type;

         bool operator==(SolidHarmonic<T, R> const &rhs)
         {
            return parent_type::m_order == rhs.m_order;
         }

         explicit SolidHarmonic(size_t order = 5, result_type arg = result_type(0.0))
               : Harmonic<T, R>(order)
         {}

         virtual bool write(std::ostream &stream)
         {
            RECONTOOLS_UINT32 order = this->order();
            stream.write(reinterpret_cast<const char *>(&order), sizeof(order));
            return true;
         }

         virtual bool read(std::istream &stream)
         {
            RECONTOOLS_UINT32 order;
            stream.read(reinterpret_cast<char *>(&order), sizeof(order));
            this->order(order);
            return true;
         }

      protected:
         virtual result_type *radial(T radius);

      };

      template<typename T, typename R = std::complex<T> >
      class HelmholtzHarmonic : public Harmonic<T, std::complex<T> >
      {
      private:
         typedef Harmonic<T, std::complex<T> > parent_type;

      public:
         bool operator==(HelmholtzHarmonic<T, R> const &rhs)
         {
            return parent_type::m_order == rhs.m_order
                   && m_k == rhs.m_k;
         }

         typedef typename parent_type::result_type result_type;

         explicit HelmholtzHarmonic(size_t order = 5, result_type k = result_type(10.0))
               : Harmonic<T, std::complex<T> >(order)
               , m_k(k)
         {}

         virtual bool write(std::ostream &stream)
         {
            RECONTOOLS_UINT32 order = static_cast<unsigned int>(parent_type::m_order);
            stream.write(reinterpret_cast<const char *>(&order), sizeof(order));
            stream.write(reinterpret_cast<const char *>(&m_k), sizeof(m_k));
            return true;
         }

         virtual bool read(std::istream &stream)
         {
            RECONTOOLS_UINT32 order;
            stream.read(reinterpret_cast<char *>(&order), sizeof(order));
            parent_type::m_order = order;
            stream.read(reinterpret_cast<char *>(&m_k), sizeof(m_k));
            return true;
         }

         void wavenumber(result_type k)
         {
            m_k = k;
         }

         result_type wavenumber() const
         {
            return m_k;
         }

      protected:
         virtual result_type *radial(T radius);

      private:
         result_type m_k;
      };

      template<typename T, typename R>
      typename HelmholtzHarmonic<T, R>::result_type *HelmholtzHarmonic<T, R>::radial(T radius)
      {
         // Spherical Bessel function of the first kind j(k*radius)
         // Algorithm based on https://doi.org/10.1016/j.cpc.2010.11.019
         size_t N = parent_type::m_order + 1;
         parent_type::m_radius.resize(N);
         result_type *radial = &(parent_type::m_radius.front());
         result_type z = radius * m_k;
         double r = std::abs(z);
         size_t n;

         if (r <= 100.0 * std::numeric_limits<double>::epsilon())
         {
            parent_type::m_radius.assign(N, 0.0);
            radial[0] = 1.0;
            return radial;
         }

         result_type zi = std::conj(z) / (r * r);
         radial[0] = std::sin(z) * zi;
         if (N < 1) return radial;
         radial[1] = radial[0] * zi - std::cos(z) * zi;
         if (N < 2) return radial;

         double sinTheta = z.imag() / r;
         size_t M = static_cast<size_t>(floor(std::min(ceil((1.83 + 4.1 * pow(sinTheta, 0.36)) * pow(r, 0.91 - 0.43 * pow(sinTheta, 0.33)) + 9.0 * (1.0 - sqrt(sinTheta))),
                                                       235.0 + 50.0 * sqrt(r))));
         M = std::max(M, N);

         result_type jmp1 = 0.0;
         result_type jm = 1e-305;
         result_type jmm1;

         for (n = M; n >= N; n--)
         {
            jmm1 = static_cast<double>((2 * n + 1)) * zi * jm - jmp1;
            jmp1 = jm;
            jm = jmm1;
         }

         while (n > 1)
         {
            radial[n] = jm;
            jmm1 = static_cast<double>((2 * n + 1)) * zi * jm - jmp1;
            jmp1 = jm;
            jm = jmm1;
            n--;
         }

         jmm1 = 3.0 * zi * jm - jmp1;
         jmp1 = jm;
         jm = jmm1;

         result_type scale;
         double ajMp1 = std::abs(jmp1);
         double ajm = std::abs(jm);
         if (ajMp1 > ajm)
         {
            jmp1 = std::conj(jmp1) / ajMp1;
            scale = (radial[1] * jmp1) / ajMp1;
            radial[0] = jm * scale;
         }
         else
         {
            jm = std::conj(jm) / ajm;
            scale = (radial[0] * jm) / ajm;
            radial[1] = jmp1 * scale;
         }

         for (n = 2; n < N; n++)
         {
            radial[n] *= scale;
         }

         return radial;
      }

      template<typename T, typename R>
      typename SolidHarmonic<T, R>::result_type *SolidHarmonic<T, R>::radial(T radius)
      {
         parent_type::m_radius.resize(parent_type::m_order + 1);
         parent_type::m_radius[0] = 1.0;
         parent_type::m_radius[1] = radius;

         // Build up the power terms of r
         // Multiply two similar size numbers each time
         for (int i = 2; i <= parent_type::m_order; i++)
            parent_type::m_radius[i] = parent_type::m_radius[i / 2] * parent_type::m_radius[i - i / 2];

         return &(parent_type::m_radius.front());
      }

   }

}

#endif //RECONTOOLS_SOLIDHARMONICS_HPP
