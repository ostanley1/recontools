//
//  ConvexHull.h
//  IceSvd
//
//  Created by Martyn Klassen on 2016-04-18.
//  Copyright © 2016 CFMM. All rights reserved.
//

#ifndef RECONTOOLS_CONVEXHULL_HPP
#define RECONTOOLS_CONVEXHULL_HPP

#include "Ellipsoid.hpp"
#include <cmath>
#include <iostream>
#include "recontools/config.hpp"
#include <utility>

namespace recontools
{
   namespace fit
   {
      class RECONTOOLS_API R3 : public P3
      {
      public:
         int id;

         R3()
               : P3(), id(-1)
         {}

         R3(const P3::value_type &a, const P3::value_type &b, const P3::value_type &x)
               : P3(a, b, x), id(-1)
         {}

         R3(const R3 &p)
               : P3(p), id(p.id)
         {}

         explicit R3(P3::value_type *d)
               : P3(d), id(-1)
         {}

         explicit R3(const P3 &p)
               : P3(p), id(-1)
         {}

         R3 &operator=(const R3 &p)
         {
            P3::operator=(p);
            id = p.id;
            return *this;
         }
      };

      struct RECONTOOLS_API Tri
      {
         int id, keep;
         int a, b, c;
         int ab, bc, ac;     // adjacent edges index to neighbouring triangle.
         P3 e;            // visible normal to triangular facet.

         Tri()
         {};

         Tri(int x, int y, int q)
               :
               id(0), keep(1), a(x), b(y), c(q), ab(-1), bc(-1), ac(-1), e()
         {};

         Tri(const Tri &p)
               :
               id(p.id), keep(p.keep), a(p.a), b(p.b), c(p.c), ab(p.ab), bc(p.bc), ac(p.ac), e(p.e)
         {}

         Tri &operator=(const Tri &p)
         {
            id = p.id;
            keep = p.keep;
            a = p.a;
            b = p.b;
            c = p.c;

            ab = p.ab;
            bc = p.bc;
            ac = p.ac;

            e = p.e;
            return *this;
         }
      };

      struct RECONTOOLS_API Facet
      {
         int a, b, c;

         Facet(int x, int y, int z)
               : a(x)
               , b(y)
               , c(z)
         {}

         Facet(const Tri &T, const std::vector<P3> &vertices)
               : a(T.a)
               , b(T.b)
               , c(T.c)
         {
            // Set ABC to be right handed pointing outward
            const P3 &A = vertices[a];
            const P3 &B = vertices[b];
            const P3 &C = vertices[c];

            if (cross(A - C, B - A) * T.e < 0)
            {
               std::swap(a, b);
            }
         }

         bool operator==(Facet const &rhs) const
         {
            return a == rhs.a &&
                   b == rhs.b &&
                   c == rhs.c;
         }
      };

      struct RECONTOOLS_API Face
      {
         P3 &A;
         P3 &B;
         P3 &C;
         P3 normal;
         P3 position;
         P3::value_type radius;
         P3 NAB;
         P3 NBC;
         P3 NCA;
         P3 AB;
         P3 BC;
         P3 CA;

         Face(P3 &a, P3 &b, P3 &c)
               : A(a)
               , B(b)
               , C(c)
         {
            // Compute the circumcenter of the face
            // m = a + (|c-a|^2 *[(b-a)x(c-a)]x(b-a) + |b-a|^2 * (c-a)x[(b-a)x(c-a)]) / (2 * |(b-a)x(c-a)|^2)
            AB = B - A;
            BC = C - B;
            CA = A - C;

            // m = A + (|-CA|^2 *[(AB)x(-CA)]x(AB) + |AB|^2 * (-CA)x[(AB)x(-CA)]) / (2 * |(AB)x(-CA)|^2)
            // A x B = - (B x A)  and |-A|^2 = |A|^2 therefore
            // m = A + (|CA|^2 *[(CA)x(AB)]x(AB) + |AB|^2 * (-CA)x[(CA)x(AB)]) / (2 * |(CA)x(AB)|^2)
            // m = A + (|CA|^2 *[(CA)x(AB)]x(AB) + |AB|^2 * [(CA)x(AB)]x(CA)) / (2 * |(CA)x(AB)|^2)

            // ABC points outwards
            normal = cross(CA, AB);

            // Compute vectors pointing into the face
            NAB = cross(normal, AB);
            NCA = cross(normal, CA);
            NBC = cross(normal, BC);

            // m = A + (|CA|^2 * NAB + |AB|^2 * NCA) / (2 * |normal|^2)

            P3::value_type ca = CA.len2();
            P3::value_type ab = AB.len2();

            // m = A + (ca * NAB + ab * NCA) * (0.5 / |normal|^2)

            // position is the vector from circumcenter to A
            position = (ca * NAB + ab * NCA) * (0.5 / normal.len2());
            radius = position.len();

            // Normalize edge vectors to simplify projection calculations
            AB /= sqrt(ab);
            CA /= sqrt(ca);
            BC /= BC.len();

            // Add A to get position of circumcenter
            position += A;
         }

         Face &operator=(const Face &f)
         {
            normal = f.normal;
            position = f.position;
            A = f.A;
            B = f.B;
            C = f.C;
            NAB = f.NAB;
            NBC = f.NBC;
            NCA = f.NCA;
            AB = f.AB;
            BC = f.BC;
            CA = f.CA;
            radius = f.radius;
            return *this;
         }

         Face(const Face &f)
               : A(f.A)
               , B(f.B)
               , C(f.C)
               , normal(f.normal)
               , position(f.position)
               , radius(f.radius)
               , NAB(f.NAB)
               , NBC(f.NBC)
               , NCA(f.NCA)
               , AB(f.AB)
               , BC(f.BC)
               , CA(f.CA)
         {}

         bool inside(const P3 &p)
         {
            return (normal * (p - position)) <= 0.0;
         }
      };

      class RECONTOOLS_API ConvexHull : public Support
      {
      public:
         ConvexHull()
               : Support()
               , m_pWork(nullptr)
         {}

         virtual ~ConvexHull()
         {}

         virtual void computePerimeter(const mask_type &mask, const int &nDim, const int *dims, const value_type *center, const value_type *voxel);

         virtual bool interior(value_type *pos)
         {
            return this->interior(reinterpret_cast<P3 *>(pos));
         }

         virtual bool interior(P3 *pos)
         { return this->interior(*pos); }

         virtual bool interior(const P3 &pos);

         virtual bool closest(P3 &pos);

         bool operator==(ConvexHull const &rhs)
         {
            return Support::operator==(rhs)
                   && m_vVertices == rhs.m_vVertices
                   && m_vFacets == rhs.m_vFacets;
         }

         virtual bool write(std::ostream &stream);

         virtual bool read(std::istream &stream);

      protected:
         typedef std::vector<Face> faces_type;
         typedef std::vector<Facet> facets_type;
         typedef std::vector<P3> vertices_type;

         virtual void setupWork(size_t numPoints);

         virtual void setPosition(value_type *offset);

         void precompute();

         facets_type m_vFacets;
         vertices_type m_vVertices;
      private:
         typedef std::vector<R3> work_type;

         faces_type m_vFaces;
         work_type *m_pWork;
      };

   }
}

#endif //RECONTOOLS_CONVEXHULL_HPP
