//
// Created by Martyn Klassen on 2019-12-06.
//

#ifndef RECONTOOLS_ELLIPSOID_HPP
#define RECONTOOLS_ELLIPSOID_HPP

#include <vector>
#include "Support.hpp"
#include "recontools/config.hpp"

namespace recontools
{
   namespace fit
   {

      class RECONTOOLS_API Ellipsoid : public Support
      {
      public:
         typedef data_type work_type;

         Ellipsoid()
               : Support()
               , m_pWork(nullptr)
         {}

         virtual ~Ellipsoid()
         {}

         virtual void computePerimeter(const mask_type &mask, const int &nDim, const int *dims, const value_type *center, const value_type *voxel);

         virtual bool interior(value_type *pos)
         { return this->interior(pos, 1.0); }

         virtual bool interior(value_type *pos, const value_type &inflation);

         const data_type &Linverse() const
         { return m_vLinverse; }

         data_type A() const;

         bool operator==(Ellipsoid const &rhs)
         {
            return Support::operator==(rhs)
                   && m_vLinverse == rhs.m_vLinverse
                   && m_vCenter == rhs.m_vCenter;
         }

         bool write(std::ostream &stream);

         bool read(std::istream &stream);

      protected:
         void compute(value_type *Q, size_t n);

         virtual void setPosition(value_type *offset);

         virtual void setupWork(size_t numPoints);

      private:
         // Data storage
         data_type m_vLinverse;
         data_type m_vCenter;

         work_type *m_pWork;
      };

   }
}

#endif //RECONTOOLS_ELLIPSOID_HPP
