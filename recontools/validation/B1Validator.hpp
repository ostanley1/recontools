//
// Created by Martyn Klassen on 2019-12-09.
//

#ifndef RECONTOOLS_B1VALIDATOR_HPP
#define RECONTOOLS_B1VALIDATOR_HPP

#include <algorithm>
#include <recontools/validation/SignalValidator.hpp>
#include <recontools/statistics/peirce.hpp>
#include <recontools/statistics/students_t.hpp>
#include <recontools/statistics/ms_shapiro_wilk.hpp>

namespace recontools
{
   namespace validation
   {
      template<class InputIt, class T>
      class B1Validator : public SignalValidator<InputIt, T>
      {
      public:
         B1Validator()
               : m_fCutoff(0.0)
               , m_fAlpha(100.0)
               , m_fAlphaSW(100.0)
               , m_fPeirceDev(-1.0)
         {}

         virtual bool isValid(T const &value, InputIt first, InputIt last)
         {

            return (value > m_fCutoff)
                   || checkSignalStatistics(first, last, m_fAlpha, m_fPeirceDev)
                   || checkShapiroWilk(first, last, m_fAlphaSW);
         }

         virtual void setStatistics(T const &cutoff, T const &alpha, size_t images, T const &alphaSW)
         {
            m_fCutoff = cutoff;
            m_fAlpha = alpha;
            m_fAlphaSW = alphaSW;
            m_fPeirceDev = images > 0 ? recontools::statistics::peirce_dev(static_cast<double>(images), 1.0, 0.0) : 0.0;
         }

      protected:
         virtual bool checkSignalStatistics(InputIt first, InputIt last, T const &alpha, T const &peirce)
         {
            size_t m = (last - first);

            if ((m <= 1) || ((alpha > 1.0 || alpha < 0.0) && peirce <= 0.0))
               return false;


            // Compute the mean
            typename InputIt::value_type mean(0.0);
            InputIt iterFirst(first);
            while (iterFirst != last)
            {
               mean += *iterFirst++;
            }
            // Divide by number of points for mean
            mean /= static_cast<typename InputIt::value_type>(m);

            // Compute the variance of the real (rVar) and imaginary (iVar) parts
            iterFirst = first;
            typename InputIt::value_type cTemp;
            T rVar(0.0);
            T iVar(0.0);
            while (iterFirst != last)
            {
               cTemp = *iterFirst++ - mean;
               rVar += cTemp.real() * cTemp.real();
               iVar += cTemp.imag() * cTemp.imag();
            }
            // Divide by the number of points minus 1 to account for one degree of freedom in mean
            rVar /= static_cast<T>(m - 1);
            iVar /= static_cast<T>(m - 1);

            // If we assume that signal varies between images, then then the samples are from different populations
            // Note that this will not work if the images are a time series, i.e. expected to be from the same population
            // If Peirce's Criteria indicates the existence of an outlier, this would suggest different populations
            // If voxel contains at least one outlier it is probably signal, not noise
            // This is not a rigorous test and is not associate with any fixed probability
            if (peirce > 0.0)
            {
               // The Peirce criteria is the that the maximum deviation is smaller than R * sample standard deviation
               // This is equivalent to square deviation < R^2 * rVar
               // peirce = R^2
               iterFirst = first;
               while (iterFirst != last)
               {
                  cTemp = *iterFirst++ - mean;
                  if ((cTemp.real() * cTemp.real()) > (peirce * rVar) || (cTemp.imag() * cTemp.imag()) > (peirce * iVar))
                     return true;
               }
            }

            // If all images are expected to have equal, normal, zero mean noise
            // Test for rejection of null hypothesis: mean value equals zero, i.e. voxel is noise
            // t = mean * sqrt(m / variance)
            return (alpha > 0.0 && alpha <= 1.0) &&
                  ((recontools::statistics::run_students_t(static_cast<int>(m), mean.real() * sqrt(static_cast<double>(m) / rVar)) < (alpha / 2.0)) ||
                   (recontools::statistics::run_students_t(static_cast<int>(m), mean.imag() * sqrt(static_cast<double>(m) / iVar)) < (alpha / 2.0)));
         }

         virtual bool checkShapiroWilk(InputIt first, InputIt last, T const &test)
         {
            if (test > 1.0)
               return false;

            // Shapiro–Wilk test for normality. Assuming noise is normal failing the normality test would indicate an
            // image voxel
            // www.matrixscience.com/msparser/help/classmatrix__science_1_1ms__shapiro_wilk.html
            recontools::statistics::matrix_science::ms_shapiro_wilk sw;
            std::vector<float> swValues;

            InputIt iterFirst(first);
            while (iterFirst != last)
            {
               swValues.push_back(static_cast<float>(iterFirst->real()));
               iterFirst++;
            }
            std::sort(swValues.begin(), swValues.end());

            for (size_t i = 0; i < swValues.size(); i++)
            {
               sw.appendSampleValue(swValues[i]);
            }

            sw.calculate(swValues.size(), swValues.size(), swValues.size() / 2);
            if (sw.getErrorCode() > 0)
            {
               return false;
            }
            else if (sw.getPValue() < test)
               return true;

            sw.clearSampleValues();
            swValues.clear();

            iterFirst = first;
            while (iterFirst != last)
            {
               swValues.push_back(static_cast<float>(iterFirst->imag()));
               iterFirst++;
            }
            std::sort(swValues.begin(), swValues.end());

            for (size_t i = 0; i < swValues.size(); i++)
            {
               sw.appendSampleValue(swValues[i]);
            }

            sw.calculate(swValues.size(), swValues.size(), swValues.size() / 2);

            return (sw.getErrorCode() <= 0) && (sw.getPValue() < test);
         }

      protected:
         T m_fCutoff;
         T m_fAlpha;
         T m_fAlphaSW;
         T m_fPeirceDev;
      };
   }
}
#endif //RECONTOOLS_B1VALIDATOR_HPP
