//
// Created by Martyn Klassen on 2019-12-09.
//

#ifndef RECONTOOLS_SIGNALVALIDATOR_HPP
#define RECONTOOLS_SIGNALVALIDATOR_HPP

namespace recontools
{
   namespace validation
   {

      template<class InputIt, class T>
      class SignalValidator
      {
      public:
         virtual bool isValid(T const &value, InputIt first, InputIt last) =0;
      };

   }
}

#endif //RECONTOOLS_SIGNALVALIDATOR_HPP
