//
// Created by Martyn Klassen on 2019-12-03.
//

#ifndef RECONTOOLS_STRIDEITERATOR_HPP
#define RECONTOOLS_STRIDEITERATOR_HPP

#include <iterator>
#include <cassert>


namespace recontools
{
   namespace iterator
   {

      template<typename Iter_T>
      class StrideIterator
      {
      public:
         // public typedefs
         typedef typename std::iterator_traits<Iter_T>::value_type value_type;
         typedef typename std::iterator_traits<Iter_T>::reference reference;
         typedef typename std::iterator_traits<Iter_T>::difference_type difference_type;
         typedef typename std::iterator_traits<Iter_T>::pointer pointer;
         typedef std::random_access_iterator_tag iterator_category;
         typedef StrideIterator self;

         // constructors
         StrideIterator()
               : m(NULL), step(0)
         {};

         StrideIterator(const self &x)
               : m(x.m), step(x.step)
         {}

         StrideIterator(Iter_T x, difference_type n)
               : m(x), step(n)
         {}

         // operators
         self &operator++()
         {
            m += step;
            return *this;
         }

         self &operator--()
         {
            m -= step;
            return *this;
         }

         self operator++(int)
         {
            self const tmp = *this;
            m += step;
            return tmp;
         }

         self operator--(int)
         {
            self const tmp = *this;
            m -= step;
            return tmp;
         }

         self &operator+=(difference_type x)
         {
            m += x * step;
            return *this;
         }

         self &operator-=(difference_type x)
         {
            m -= x * step;
            return *this;
         }

         reference operator[](difference_type n)
         { return m[n * step]; }

         reference operator*()
         { return *m; }

         pointer operator->()
         {
            return &(operator*());
         }

         // friend operators
         friend bool operator==(const self &x, const self &y)
         {
            assert(x.step == y.step);
            return x.m == y.m;
         }

         friend bool operator!=(const self &x, const self &y)
         {
            assert(x.step == y.step);
            return x.m != y.m;
         }

         friend bool operator<(const self &x, const self &y)
         {
            assert(x.step == y.step);
            return x.m < y.m;
         }

         friend difference_type operator-(const self &x, const self &y)
         {
            assert(x.step == y.step);
            return (x.m - y.m) / x.step;
         }

         friend self operator+(const self &x, difference_type y)
         {
            return x += y * x.step;
         }

         friend self operator+(difference_type x, const self &y)
         {
            return y += x * y.step;
         }

      private:
         Iter_T m;
         difference_type step;
      };

   }

}

#endif //RECONTOOLS_STRIDEITERATOR_HPP
