//
// Created by Martyn Klassen on 2019-12-05.
//

#ifndef RECONTOOLS_CONFIG_HPP
#define RECONTOOLS_CONFIG_HPP

#ifdef IDEA
   // Make the obsolete C++ compiler required by IDEA not behave completely rediculously
#  define nullptr NULL
#  define RECONTOOLS_MOVE(VAR) VAR

#  if defined(WIN32)
      typedef unsigned __int32 RECONTOOLS_UINT32;

#     define RECONTOOLS_VECTOR_POINTER(VAR) &(VAR.front())
#  else
#     include <stdint.h>
      typedef uint32_t RECONTOOLS_UINT32;
#  endif

#else
#  include <cstdint>
   typedef uint32_t RECONTOOLS_UINT32;
#endif

#ifndef RECONTOOLS_VECTOR_POINTER
#  define RECONTOOLS_VECTOR_POINTER(VAR) VAR.data()
#endif

#ifndef RECONTOOLS_MOVE
#  include <utility>
#  define RECONTOOLS_MOVE(VAR) std::move(VAR)
#endif


#if defined(WIN32)

# ifdef RECONTOOLS_BUILD
#     define RECONTOOLS_API __declspec(dllexport)
#  else
#     define RECONTOOLS_API __declspec(dllimport)
#  endif

#else

#  define RECONTOOLS_API

#endif

#endif //RECONTOOLS_CONFIG_HPP
