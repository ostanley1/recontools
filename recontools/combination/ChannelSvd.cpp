//
// Created by Martyn Klassen on 2019-12-03.
//

#include <numeric>
#include <limits>
#include <algorithm>
#include "recontools/config.hpp"
#include "recontools/combination/ChannelSvd.hpp"
#include <cpplapack.h>
#include <iostream>

namespace recontools
{
   namespace combination
   {

      ChannelSvd::ChannelSvd()
            : m_pValidator(nullptr)
            , m_lImages(2)
            , m_lChannels(2)
            , m_lK(2)
            , m_lWork(-1)
            , m_cJobU('N')
      {
         setSize(2,2);
      }

      ChannelSvd::ChannelSvd(size_t channels, size_t images, bool computeB1, validator *pValidator)
            : m_pValidator(pValidator)
            , m_lImages(images)
            , m_lChannels(channels)
            , m_lK(std::min(m_lChannels, m_lImages))
            , m_lWork(-1)
            , m_cJobU(computeB1 ? 'S' : 'N')
            , m_vA(m_lChannels * m_lImages)
            , m_vU(0)
            , m_vS(m_lK)
            , m_vrWork(5 * m_lK)
      {
         setSize(channels, images);
      }

      void ChannelSvd::setSize(size_t channels, size_t images)
      {
         m_lChannels = channels;
         m_lImages = images;
         m_lK = std::min(m_lChannels, m_lImages);

         m_vA.resize(m_lChannels * m_lImages);

         computeB1(m_cJobU == 'S');

         // Setup for SVD
         int lWork = -1;

         complex_type scale;

         // Query for the amount of workspace required
         cpplapack::gesvd(m_cJobU, 'O',
                          m_lChannels, m_lImages, nullptr, m_lChannels,
                          nullptr,
                          nullptr, m_lChannels,
                          nullptr, m_lK,
                          &scale, lWork, nullptr);

         lWork = static_cast<int>(*(reinterpret_cast<real_type *> (&scale)));
         m_vWork.resize(static_cast<size_t>(lWork));
      }

      void ChannelSvd::computeB1(bool status)
      {
         if (status)
         {
            m_cJobU = 'S';
            m_vU.resize(m_lK * m_lChannels);
         }
         else
         {
            m_cJobU = 'N';
            m_vU.clear();
         }
      }

      size_t ChannelSvd::size() const
      {
         return m_vA.size();
      }

      ChannelSvd::complex_type ChannelSvd::isolatePhase(complex_type const &value)
      {
         double mag(hypot(value.real(), value.imag()));
         if (mag < std::numeric_limits<real_type>::epsilon() * 1e3)
            return 1.0;
         else
            return value / mag;
      }

      ChannelSvd::iterator ChannelSvd::input()
      {
         return iterator(m_vA.begin(), 1);
      }

      ChannelSvd::const_iterator ChannelSvd::begin() const
      {
         return const_iterator(m_vA.begin(), m_lChannels);
      }

      ChannelSvd::const_iterator ChannelSvd::end() const
      {
         return const_iterator(m_vA.end(), m_lChannels);
      }

      ChannelSvd::real_type ChannelSvd::snr() const
      {
         return m_vA[1].real();
      }

      ChannelSvd::real_type ChannelSvd::sos() const
      {
         return m_vA[1+m_lChannels].real();
      }

      ChannelSvd::complex_array_type const &ChannelSvd::b1() const
      {
         return m_vU;
      }

      ChannelSvd::complex_type ChannelSvd::computeReference()
      {
         // Siemens uses a complex sum to get the phase images
         // We will arbitrarily use the first image to set phase
         // Generally we are doing multi-echo and the first image
         // will have the highest SNR
         // We could alternately try to find the image with the
         // highest SNR, but that is a lot of extra work for
         // something that is ultimately just a relative offset
         // Isolate the phase, ie remove the magnitude
         return isolatePhase(std::accumulate(m_vA.begin(), m_vA.begin() + m_lChannels, complex_type(0.0)));
      }

      void ChannelSvd::setValidator(validator *pValidator)
      {
         m_pValidator = pValidator;
      }

      void ChannelSvd::performSVD()
      {
         complex_type scale = computeReference();

         // Perform the SVD
         // m_vU is based for both u and vt
         // vt overwrites a (value passed to vt is ignored)
         // u is written to m_vU
         cpplapack::gesvd(m_cJobU, 'O',
                          m_lChannels, m_lImages,
                          m_vA, m_lChannels,
                          m_vS,
                          m_vU, m_lChannels,
                          m_vU /* not used */, m_lK,
                          m_vWork, static_cast<int>(m_vWork.size()),
                          m_vrWork);

         // m_vA now holds vt, i.e. image data

         // The first singular value is an estimate of the signal
         // The second singular value is an estimate of noise/artifact
         // Compute the SNR and store in the unused second CHA of the first image
         // Use float limits prevent overflow if doubles are converted back to floats
         if (fabs(m_vS[1]) > std::numeric_limits<float>::epsilon() * 1e3)
            m_vA[1] = fabs(m_vS[0] / m_vS[1]);
         else
            m_vA[1] = std::numeric_limits<float>::max();

         // Store the SoS equivalent image in the second CHA of the second image
         m_vA[m_lChannels + 1] = m_vS.front();

         // Get phase of first image
         complex_type firstImagePhase(isolatePhase(m_vA.front()));

         // Extract the B1- (receiver) information
         if ('S' == m_cJobU)
         {
            // Use the callback function to check for valid signal
            // Weight the B1 maps by the sqrt of the SoS image and add the phase of the first image
            // Weighting is to improve fitting. The l2 norm of the nominal B1 maps is always 1.0, so
            // the weighting can be removed later.
            complex_type weight = (nullptr == m_pValidator)
                  || m_pValidator->isValid(m_vA[1].real(), const_iterator(m_vA.begin(), m_lChannels), const_iterator(m_vA.end(), m_lChannels))
                  ? sqrt(m_vS.front()) * firstImagePhase : 0.0;

            // Apply the weighting and copy the B1 maps to output iterator
            complex_array_type::iterator iterStart(m_vU.begin());
            complex_array_type::iterator iterStop(m_vU.begin() + m_lChannels);
            while(iterStart != iterStop)
            {
               *iterStart++ *= weight;
            }
         }

         // Remove the phase in the first element of vt
         // and add phase of the first image (stored in scale)
         scale *= std::conj(firstImagePhase);

         // Include first singular value in images
         scale *= m_vS[0];

         // Scale CHA=0 images to set the correct phase and amplitude
         iterator iterStart(m_vA.begin(), m_lChannels);
         iterator iterStop(m_vA.end(), m_lChannels);

         while (iterStart != iterStop)
         {
            *iterStart++ *= scale;
         }
      }

   }
}
