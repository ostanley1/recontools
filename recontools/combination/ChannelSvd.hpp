//
// Created by Martyn Klassen on 2019-12-03.
//

#ifndef RECONTOOLS_CHANNELSVD_HPP
#define RECONTOOLS_CHANNELSVD_HPP

#include <vector>
#include <complex>
#include "recontools/iterator/StrideIterator.hpp"
#include "recontools/validation/SignalValidator.hpp"
#include "recontools/config.hpp"

namespace recontools
{
   namespace combination
   {
      class RECONTOOLS_API ChannelSvd
      {
      public:
         typedef double real_type;
         typedef std::complex<real_type> complex_type;
         typedef std::vector<complex_type> complex_array_type;
         typedef std::vector<real_type> real_array_type;
         typedef recontools::iterator::StrideIterator <complex_array_type::iterator> iterator;
         typedef recontools::iterator::StrideIterator <complex_array_type::const_iterator> const_iterator;
         typedef recontools::validation::SignalValidator<const_iterator, double> validator;

         ChannelSvd();

         ChannelSvd(size_t channels, size_t images, bool computeB1, validator * pValidator);

         virtual ~ChannelSvd() {};

         iterator input();
         const_iterator begin() const;
         const_iterator end() const;
         real_type snr() const;
         real_type sos() const;
         complex_array_type const &b1() const;

         virtual void performSVD();

         void setValidator(validator *pValidator);

         void setSize(size_t channels, size_t images);

         void computeB1(bool status);

         size_t size() const;

      protected:
         virtual complex_type computeReference();

      private:
         validator *m_pValidator;

         static complex_type isolatePhase(complex_type const &scale);

         size_t m_lImages;
         size_t m_lChannels;
         size_t m_lK;
         long m_lWork;

         char m_cJobU;

         complex_array_type m_vA;
         complex_array_type m_vU;
         complex_array_type m_vWork;
         real_array_type m_vS;
         real_array_type m_vrWork;
      };

   }
}

#endif //RECONTOOLS_CHANNELSVD_HPP
