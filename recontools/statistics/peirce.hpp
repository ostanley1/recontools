//
// Created by Martyn Klassen on 2019-12-03.
//

#ifndef RECONTOOLS_PEIRCE_HPP
#define RECONTOOLS_PEIRCE_HPP
#include "recontools/config.hpp"

namespace recontools
{
   namespace statistics
   {

      /*
      Name:     peirce_dev
      Input:    - int, total number of observations (N)
                - int, number of outliers to be removed (n)
                - int, number of model unknowns (m)
      Output:   float, squared error threshold (x2)
      Features: Returns the squared threshold error deviation for outlier
                identification using Peirce's criterion based on Gould's
                methodology
      */
      RECONTOOLS_API double peirce_dev(double N, double n, double m);

   }

}

#endif //RECONTOOLS_PEIRCE_HPP
