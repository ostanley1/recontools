#ifndef RECONTOOLS_MS_SHAPIRO_WILK_HPP
#define RECONTOOLS_MS_SHAPIRO_WILK_HPP

/*
##############################################################################
# File: ms_shapiro_wilk.hpp                                                  #
# Mascot Parser toolkit                                                      #
# Include file for Shapiro-Wilk W test (statistical algorithm)               #
##############################################################################
#    $Source: /vol/cvsroot/parser/inc/ms_shapiro_wilk.hpp,v $
#    $Author: martins $
#      $Date: 2014/02/07 17:07:04 $
#  $Revision: 1.6 $
##############################################################################
*/
/*
##############################################################################
# Applied Statistics algorithms                                              #
#                                                                            #
# Translated and adapted from routines that originally appeared in the       #
# Journal of the Royal Statistical Society Series C (Applied Statistics)     #
# and made available through StatLib http://lib.stat.cmu.edu/                #
#                                                                            #
# The implementation and source code for this algorithm is available as a    #
# free download from http://www.matrixscience.com/msparser.html              #
# to conform with the requirement that no fee is charged for their use       #
##############################################################################
*/

// Includes from the standard template library
#include <string>
#include <deque>
#include <list>
#include <vector>
#include <set>
#include <map>
#include "recontools/config.hpp"

namespace recontools
{
   namespace statistics
   {

      namespace matrix_science
      {

         class RECONTOOLS_API ms_shapiro_wilk
         {
         public:
            ms_shapiro_wilk();

            ~ms_shapiro_wilk() {};

            ms_shapiro_wilk(std::deque<std::pair<size_t, double> > x, long n, long n1, long n2);

            void calculate(long n, long n1, long n2);

            void appendSampleValue(double y);

            void clearSampleValues();

            double getResult() const;

            double getPValue() const;

            double getErrorCode() const;

         private:
            bool init_;
            double w_;       // The Shapiro-Wilks W-statistic.
            double pw_;      // the P-value for w
            int ifault_;  // error indicator
            std::deque<double> a_;
            std::deque<std::pair<size_t, double> > x_;

         public:
            static void swilk(bool init, std::deque<std::pair<size_t, double> > x, long n, long n1, long n2, std::deque<double> &a, double &w, double &pw, int &ifault);

         private:
            static double poly(const double *cc, int nord, double x);

            static double ppnd7(double p, int &ifault);

            static double alnorm(double x, bool upper);

            static long sign(long x, long y);
         };
         // end of quantitation_group
      } // matrix_science

   }
}

#endif //RECONTOOLS_MS_SHAPIRO_WILK_HPP
