//
// Created by Martyn Klassen on 2019-12-05.
//

#ifndef RECONTOOLS_STUDENTS_T_HPP
#define RECONTOOLS_STUDENTS_T_HPP
#include "recontools/config.hpp"

namespace recontools
{
   namespace statistics
   {
      RECONTOOLS_API double run_students_t(int k, double x);
   }
}

#endif //RECONTOOLS_STUDENTS_T_HPP
