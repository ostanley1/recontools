#include "gtest/gtest.h"
#include <recontools/fit/Minimax.cpp>
#include "H5Cpp.h"
#include "math.h"

#define ASSERT_FLOATS_NEARLY_EQ(expected, actual, thresh) \
       ASSERT_EQ(expected.size(), actual.size()) << "Array sizes differ.";\
        for (size_t idx = 0; idx < std::min(expected.size(), actual.size()); ++idx) \
        { \
            ASSERT_NEAR(expected[idx], actual[idx], thresh) << "at index: " << idx;\
        }

std::vector<double> ReadArrayFromMatFile(string input, H5::H5File file) {
    H5::DataSet dataset = file.openDataSet(input.c_str());
    H5::DataSpace dataspace = dataset.getSpace();
    hsize_t dims_out[2];
    dataspace.getSimpleExtentDims( dims_out, NULL);

    std::vector<double> data(*dims_out);
    dataset.read(data.data(), H5::PredType::NATIVE_DOUBLE, dataspace, dataspace);
    return data;
    }

void calcMeanandStd(std::vector<double> & vec, double * mean, double * std) {
    for (int i=0; i <vec.size(); i++) {
        *mean += vec[i];
    }
    *mean = *mean/vec.size();
    for (int j=0; j <vec.size(); j++) {
        *std += (vec[j] - *mean)*(vec[j] - *mean);
    }
    *std = sqrt(*std/vec.size());
}

void RunMatlabTestset(std::string filename, int Ntest, int npointstest, int nweightstest) {
    string Nstring = "/N";
    string cwstring = "/cw";
    string iwstring = "/iw";
    string datastring = "/cd";
    string minstring = "/minval";

    H5::H5File file(filename.c_str(), H5F_ACC_RDONLY);

    // Read in number of coils
    H5::DataSet Ndataset = file.openDataSet(Nstring.c_str());
    H5::DataSpace dataspace = Ndataset.getSpace();
    int N;
    Ndataset.read(&N, H5::PredType::NATIVE_INT, dataspace, dataspace);
    std::cout << "Number of coils in test: "<< N << std::endl;
    ASSERT_EQ(N, Ntest);

    // Read in data for test
    hsize_t dims_out[2];
    std::vector<double> coildata = ReadArrayFromMatFile(datastring, file);
    std::cout << "Coil data in " << std::flush;
    for (int i=0; i<12; i++) {
        std::cout << coildata[i] << " " << std::flush;
    }
    std::cout << std::endl;
    int n_points = coildata.size()/N/2;
    ASSERT_EQ(n_points, npointstest);

    // Read in SVD solution
    std::vector<double> svddata = ReadArrayFromMatFile(iwstring, file);
    std::cout << "SVD data in " << std::flush;
    for (int i=0; i<4; i++) {
        std::cout << svddata[i] << " " << std::flush;
    }
    std::cout << std::endl;

    // Read in test solution
    std::vector<double> weightdata = ReadArrayFromMatFile(cwstring, file);
    ASSERT_EQ(weightdata.size(), nweightstest);

    // Run test
    auto * dataptr = reinterpret_cast<std::complex<double> *>(coildata.data());
    std::vector<std::complex<double> > data (dataptr, dataptr+n_points*N);
    std::vector<double> weights(N*2);
    weights[0] = 1;
    int result = 1;
    result = RunSVD(data, N, n_points, weights);
    ASSERT_TRUE(result>0);
    ASSERT_FLOATS_NEARLY_EQ(weights, svddata, 0.01);
    if (result > 0) {
        result = RunMinimax(data, N, n_points, weights);
    }
    ASSERT_TRUE(result>0);
    ASSERT_FLOATS_NEARLY_EQ(weights, weightdata, 0.01);

    // Check maximum value of combo is > 0 and compare to matlab
    auto * weightptr = reinterpret_cast<std::complex<double> *>(weights.data());
    std::vector<double> magimage(n_points);
    double maximin = 10000;
    for (int p=0; p < n_points; p++) {
        complex<double> pointval = 0.0 + 0.0i;
        for (int c=0; c < N; c++) {
            pointval += *(dataptr + p * N + c) * *(weightptr + c);
        }
        magimage[p] = std::abs(pointval);
        if (magimage[p] < maximin) {
            maximin = std::abs(pointval) ;
        }
    }
    ASSERT_NE(maximin, 0);
    double mean, std;
    calcMeanandStd(magimage, &mean, &std);
    std::cout << "Minimum value across points: " << maximin << std::endl;
    std::cout << "Mean value across points: " << mean << std::endl;
    std::cout << "Std dev value across points: " << std << std::endl;

    // Compare minval to matlab minval
    H5::DataSet mindataset = file.openDataSet(minstring.c_str());
    dataspace = mindataset.getSpace();
    double minval;
    mindataset.read(&minval, H5::PredType::NATIVE_DOUBLE, dataspace, dataspace);
    EXPECT_NEAR(minval, maximin, 0.1);

}

//TEST(MinimaxTestSuite, AssertTest) {
//    // test that build can run tests
//    EXPECT_EQ(1,1);
//}

TEST(MinimaxTestSuite, NullObjectiveTest) {
    // test the objective function and grad
    std::vector<double> weights = {1, 1, 1, 1, 1};
    double result = ObjectiveMinimax(5, weights.data(), NULL, NULL);
    ASSERT_FLOAT_EQ(result, 1);
}

TEST(MinimaxTestSuite, ObjectiveTest) {
    // test the objective function and grad
    std::vector<double> weights = {1, 1, 1, 1, 1};
    std::vector<double> grad = {0, 1, 0, 0, 0};
    std::vector<double> gradanswer = {1, 0, 0, 0, 0};
    double result = ObjectiveMinimax(5, weights.data(), grad.data(), NULL);
    ASSERT_FLOAT_EQ(result, 1);
    ASSERT_FLOATS_NEARLY_EQ(grad, gradanswer, 0.01);
}

TEST(MinimaxTestSuite, NullNormConstraintTest) {
    // test the objective function and grad
    std::vector<double> weights = {1, 1, 1, 1, 1};
    double result = NormConstraintMinimax(5, weights.data(), NULL, NULL);
    ASSERT_FLOAT_EQ(result, 4-1);
}

TEST(MinimaxTestSuite, NormConstraintTest) {
    // test the objective function and grad
    std::vector<double> weights = {1, 1, 1, 1, 1};
    std::vector<double> grad = {0, 1, 0, 0, 0};
    std::vector<double> gradanswer = {0, 2, -2, 2, -2};
    double result = NormConstraintMinimax(5, weights.data(), grad.data(), NULL);
    ASSERT_FLOAT_EQ(result, 4-1);
    ASSERT_FLOATS_NEARLY_EQ(grad, gradanswer, 0.01);
}

TEST(MinimaxTestSuite, NullPointConstraintTest) {
    // test the objective function and grad
    std::vector<double> weights = {1, 1, 1, 1, 1};
    std::vector<double> data = {1, 0, 1, 0};
    double result = PointConstraintMinimax(5, weights.data(), NULL, reinterpret_cast<void *> (data.data()));
    ASSERT_FLOAT_EQ(result, -7);
}

TEST(MinimaxTestSuite, PointConstraintTest) {
    // test the objective function and grad
    std::vector<double> weights = {1, 1, 1, 1, 1};
    std::vector<double> data = {1, 0, 1, 0};
    std::vector<double> grad = {0, 1, 0.5, 0, 0};
    std::vector<double> gradanswer = {0, 8, 4, 0, 0};
    double result = PointConstraintMinimax(5, weights.data(), grad.data(), reinterpret_cast<void *> (data.data()));
    ASSERT_FLOAT_EQ(result, -7);
    ASSERT_FLOATS_NEARLY_EQ(grad, gradanswer, 0.01);
}

TEST(MinimaxTestSuite, BasicTest){
    // run a simple test with 2 coils both real valued
    int n_coils = 2;
    int n_points = 3;
    std::vector<double> weights(n_coils * 2, 0);
    weights[1] = 1;
    std::vector<std::complex<double> > data(n_points*n_coils);
    std::complex<double> temp(0.5,0.5);
    for (int i = 0; i < 6; i++) {
        data[i] = temp;
    }
    int result = 1;
    result = RunSVD(data, n_coils, n_points, weights);
    std::vector<double> trueweightsout { -0.707107, 0, -0.707107, -0 };
    ASSERT_FLOATS_NEARLY_EQ(weights, trueweightsout, 0.001);
    if (result > 0) {
        result = RunMinimax(data, n_coils, n_points, weights);
    }
    ASSERT_TRUE(result>0);
    ASSERT_FLOATS_NEARLY_EQ(weights, trueweightsout, 0.001);

    // Check maximum value of combo is > 0 and compare to matlab
    auto * dataptr = reinterpret_cast<std::complex<double> *>(data.data());
    auto * weightptr = reinterpret_cast<std::complex<double> *>(weights.data());
    std::vector<double> magimage(n_points);
    double maximin = 10000;
    for (int p=0; p < n_points; p++) {
        complex<double> pointval = 0.0 + 0.0i;
        for (int c=0; c < n_coils; c++) {
            pointval += *(dataptr + p * n_coils + c) * *(weightptr + c);
        }
        magimage[p] = std::abs(pointval);
        cout << magimage[p] << " " << flush;
        if (magimage[p] < maximin) {
            maximin = std::abs(pointval) ;
        }
    }
    cout << endl;
    ASSERT_NE(maximin, 0);
    double mean, std;
    calcMeanandStd(magimage, &mean, &std);
    std::cout << "Minimum value across points: " << maximin << std::endl;
    std::cout << "Mean value across points: " << mean << std::endl;
    std::cout << "Std dev value across points: " << std << std::endl;
}

//TEST(MinimaxTestSuite, RealandImaginaryTest){
//    // run a simple test with 2 coils one real valued, one imaginary
//    int n_coils = 2;
//    int n_points = 3;
//    std::vector<double> weights(n_coils * 2);
//    std::vector<std::complex<double> > data(n_points*n_coils);
//    std::complex<double> temp(1, 0);
//    std::complex<double> imagtemp(0, 1);
//    for (int i = 0; i < 6; i++) {
//        if (i % 2 == 0) {
//        data[i] = temp;
//        }
//        else {
//            data[i] = imagtemp;
//        }
//    }
//    int result = RunSVD(data, n_coils, n_points, weights);
//    if (result > 0) {
//        result = RunMinimax(data, n_coils, n_points, weights);
//    }
//    ASSERT_TRUE(result>0);
//    std::vector<double> trueweightsout { -0.707107, 0, 0, -0.707107 };
//    ASSERT_FLOATS_NEARLY_EQ(weights, trueweightsout, 0.001);
//}
//
//TEST(MinimaxTestSuite, ReadInTest) {
//    // Read in basic test from MATLAB file and run
//    string filename = "./testcases/ReadInTest.mat";
//    RunMatlabTestset(filename, 2, 3, 4);
//}
//
//TEST(MinimaxTestSuite, TwoCoilTest) {
//    // Read in basic test from MATLAB file and run
//    string filename = "./testcases/TwoCoilTest.mat";
//    RunMatlabTestset(filename, 2, 3, 4);
//}
//
//TEST(MinimaxTestSuite, NoiseyDataTest) {
//    // Read in basic test from MATLAB file and run
//    string filename = "./testcases/DataWithNoiseTest.mat";
//    RunMatlabTestset(filename, 2, 3, 4);
//}
//
//TEST(MinimaxTestSuite, MoreNoiseyDataTest) {
//    // Read in basic test from MATLAB file and run
//    string filename = "./testcases/DataWithLotsOfNoiseTest.mat";
//    RunMatlabTestset(filename, 2, 3, 4);
//}

//TEST(MinimaxTestSuite, PhantomTest) {
//    // Read in basic test from MATLAB file and run
//    string filename = "./testcases/PhantomTest.mat";
//    RunMatlabTestset(filename, 32, 9142, 64);
//}