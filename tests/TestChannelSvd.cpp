//
// Created by Martyn Klassen on 2019-12-05
//

#include "gmock/gmock.h"

#pragma clang diagnostic push
#pragma ide diagnostic ignored "CannotResolve"

#include "recontools/iterator/StrideIterator.hpp"
#include "recontools/combination/ChannelSvd.hpp"
#include "recontools/validation/B1Validator.hpp"
#include <cpplapack/cpplapack.h>
#include <random>

namespace
{

   class ChannelSvdTest : public ::testing::Test
   {
   };

   TEST_F(ChannelSvdTest, noB1) // NOLINT
   {
      size_t nCoils(32);
      size_t nImages(10);

      recontools::validation::B1Validator<recontools::combination::ChannelSvd::const_iterator, double> oValidator;
      recontools::combination::ChannelSvd obj(nCoils, nImages, false, &oValidator);

      std::vector<std::complex<double>> sensitivity(nCoils);
      std::vector<std::complex<double>> images(nImages);
      std::vector<std::complex<double>> result(nCoils * nImages);

      std::vector<std::complex<double>> data;

      std::random_device rd{};
      std::mt19937 gen{rd()};

      std::normal_distribution<> d{10.0, 2.0};

      for (int i = 0; i < nCoils; i++)
      {
         sensitivity[i] = std::complex<double>(d(gen), d(gen));
      }
      for (int i = 0; i < nImages; i++)
      {
         images[i] = std::complex<double>(d(gen), d(gen));
      }

      cpplapack::gemm('N', 'T', nCoils, nImages, 1lu, 1.0, sensitivity, nCoils, images, nImages, 0.0, result, nCoils);
      std::copy(result.begin(), result.end(), obj.input());

      obj.performSVD();

      double magnitudeSensitivity = cpplapack::nrm2(nCoils, sensitivity, 1lu);

      double magnitudeImages = cpplapack::nrm2(nImages, images, 1lu);

      // The SoS values should be the norm of the sensitivity and images
      EXPECT_NEAR(obj.sos(), magnitudeImages * magnitudeSensitivity, 1e-10);

      // Value of the first image
      std::complex<double> meanFirstImage = images[0] * std::accumulate(sensitivity.begin(), sensitivity.end(), std::complex<double>(0.0));

      auto iterData(obj.begin());
      auto iterEnd(obj.end());
      auto iterCmp = images.begin();
      // Add the sensitivity magnitude
      // Remove the phase of the first image
      // Add the phase of the complex sum of first images
      auto scale = magnitudeSensitivity * std::conj(images[0]) / std::abs(images[0]) * meanFirstImage / std::abs(meanFirstImage);
      while (iterData != iterEnd)
      {
         std::complex<double> value = *iterCmp * scale;

         // Check that the values agree
         EXPECT_NEAR(iterData->real(), value.real(), 1e-11);
         EXPECT_NEAR(iterData->imag(), value.imag(), 1e-11);

         iterData++;
         iterCmp++;
      }
   }

   TEST_F(ChannelSvdTest, B1) // NOLINT
   {
      size_t nCoils(32);
      size_t nImages(10);

      recontools::validation::B1Validator<recontools::combination::ChannelSvd::const_iterator, double> oValidator;
      recontools::combination::ChannelSvd obj(nCoils, nImages, true, &oValidator);

      std::vector<std::complex<double>> sensitivity(nCoils);
      std::vector<std::complex<double>> images(nImages);
      std::vector<std::complex<double>> result(nCoils * nImages);

      std::vector<std::complex<double>> data;

      std::random_device rd{};
      std::mt19937 gen{rd()};

      std::normal_distribution<> d{10.0, 2.0};

      for (size_t i = 0; i < nCoils; i++)
      {
         sensitivity[i] = std::complex<double>(d(gen), d(gen));
      }
      for (size_t i = 0; i < nImages; i++)
      {
         images[i] = std::complex<double>(d(gen), d(gen));
         images[i] *= std::conj(images[0]) / std::abs(images[0]);
      }


      cpplapack::gemm('N', 'T', nCoils, nImages, 1lu, 1.0, sensitivity, nCoils, images, nImages, 0.0, result, nCoils);
      std::copy(result.begin(), result.end(), obj.input());

      obj.performSVD();

      double magnitudeSensitivity = cpplapack::nrm2(nCoils, sensitivity, 1lu);

      double magnitudeImages = cpplapack::nrm2(nImages, images, 1lu);

      // The SoS values should be the norm of the sensitivity and images
      EXPECT_NEAR(obj.sos(), magnitudeImages * magnitudeSensitivity, 1e-10);

      // Value of the first image
      std::complex<double> meanFirstImage = images[0] * std::accumulate(sensitivity.begin(), sensitivity.end(), std::complex<double>(0.0));

      auto iterData(obj.begin());
      auto iterEnd(obj.end());
      auto iterCmp = images.begin();
      // Add the sensitivity magnitude
      // Remove the phase of the first image
      // Add the phase of the complex sum of first images
      auto scale = magnitudeSensitivity * std::conj(images[0]) / std::abs(images[0]) * meanFirstImage / std::abs(meanFirstImage);
      while (iterData != iterEnd)
      {
         std::complex<double> value = *iterCmp * scale;
         // Check that the values agree
         EXPECT_NEAR(iterData->real(), value.real(), 1e-11);
         EXPECT_NEAR(iterData->imag(), value.imag(), 1e-11);

         iterData++;
         iterCmp++;
      }

      auto iterSensitivity(obj.b1().begin());
      auto iterEndSensitivity(iterSensitivity + nCoils);
      iterCmp = sensitivity.begin();
      // Add phase of the first image
      // Normalize the magnitude to 1
      // Add the SoS weighting
      scale = (images[0] / std::abs(images[0])) / magnitudeSensitivity * sqrt(magnitudeImages * magnitudeSensitivity);
      while (iterSensitivity != iterEndSensitivity)
      {
         std::complex<double> value = *iterCmp * scale;

         // Check that the values agree
         EXPECT_NEAR(iterSensitivity->real(), value.real(), 1e-11);
         EXPECT_NEAR(iterSensitivity->imag(), value.imag(), 1e-11);

         iterSensitivity++;
         iterCmp++;
      }
   }
}