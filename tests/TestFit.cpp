//
// Created by Martyn Klassen on 2019-12-18.
//

//
// Created by Martyn Klassen on 2019-12-17.
//

#include "gmock/gmock.h"

#pragma clang diagnostic push
#pragma ide diagnostic ignored "CannotResolve"

#include <recontools/fit/B1Fit.hpp>
#include <recontools/fit/SolidHarmonics.hpp>
#include <random>

namespace
{

   class FitTest : public ::testing::Test
   {
   };


   TEST_F(FitTest, initialization) // NOLINT
   {
      recontools::fit::B1Fit<recontools::fit::HelmholtzHarmonic<double> > b1Fit;
      recontools::fit::B1Fit<recontools::fit::SolidHarmonic<double> > b1Fit2;
   }

   TEST_F(FitTest, serialization) // NOLINT
   {
      typedef recontools::fit::B1Fit<recontools::fit::SolidHarmonic<double>, recontools::fit::ConvexHull > fit_type;
      fit_type b1Fit;

      size_t nCoils = 10;
      size_t nCoefficients = nCoils * b1Fit.basis().terms();

      fit_type::data_type vCoefficients(nCoefficients);

      std::random_device rd;
      std::mt19937 engine{rd()};

      std::normal_distribution<double>dist(0.0, 3.0);

      std::generate(vCoefficients.begin(), vCoefficients.end(), [&dist, &engine](){ return fit_type::value_type(dist(engine), dist(engine));});

      b1Fit.coefficients(nCoils, vCoefficients.data());

      fit_type newB1Fit;

      EXPECT_FALSE(b1Fit == newB1Fit);

      std::stringstream data;
      ASSERT_TRUE(b1Fit.write(data));
      newB1Fit.read(data);
      EXPECT_TRUE(b1Fit == newB1Fit);
   }
}