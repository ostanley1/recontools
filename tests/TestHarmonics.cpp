//
// Created by Martyn Klassen on 2019-12-17.
//

#include "gmock/gmock.h"

#pragma clang diagnostic push
#pragma ide diagnostic ignored "CannotResolve"

#include <recontools/fit/SolidHarmonics.hpp>
#include <recontools/fit/B1Fit.hpp>
#include <boost/math/special_functions/spherical_harmonic.hpp>

namespace
{

   template<class T>
   class SphericalHarmonicTest : public recontools::fit::Harmonic<T, T>
   {
   private:
      typedef recontools::fit::Harmonic<T, T> parent_type;

   protected:
      typename parent_type::result_type *radial(T radius) override
      {
         // Radius is always one for testing spherical harmonics
         parent_type::m_radius.assign(parent_type::m_order + 1, 1.0);
         return parent_type::m_radius.data();
      }
   };

   template<class T>
   class RadialTest : public T
   {
   public:
      typename T::data_type getRadial(typename T::value_type radius)
      {
         T::radial(radius);
         return T::m_radius;
      }
   };

   class HarmonicsTest : public ::testing::Test
   {
   protected:
      void SetUp() override
      {
         m_vPosition = {0.0, 0.0, 0.0};
      }

      template<typename T>
      void checkValue(T &value1, T &value2, T &error) const
      {
         EXPECT_NEAR(value1, value2, error) << m_ssMsg.str();
      }

      template<typename T>
      void checkValue(std::complex<T> &value1, std::complex<T> &value2, T &error) const
      {
         EXPECT_NEAR(value1.real(), value2.real(), error) << m_ssMsg.str();
         EXPECT_NEAR(value1.imag(), value2.imag(), error) << m_ssMsg.str();
      }

      template<class B>
      void checkPosition(B &basis, typename B::value_type (*radial)(typename B::value_type, typename B::value_type))
      {
         auto phi = atan2(m_vPosition[1], m_vPosition[0]);
         if (phi < 0.0)
            phi += M_PI;
         auto theta = atan2(sqrt(m_vPosition[0] * m_vPosition[0] + m_vPosition[1] * m_vPosition[1]), m_vPosition[2]);

         auto radius = sqrt(m_vPosition[0] * m_vPosition[0] + m_vPosition[1] * m_vPosition[1] + m_vPosition[2] * m_vPosition[2]) / 1000.0;

         typename B::data_type result(basis.terms(), 0.0);
         basis.compute(m_vPosition.data(), result.data());


         // Center is special
         typename B::value_type error(1e-15);
         if (radius == 0.0)
         {
            auto a = sqrt(0.25 / M_PI);
            newMessage() << "Center failure at 0";
            checkValue(result[0], a, error);

            a = 0.0;
            for (size_t i = 1; i < basis.terms(); i++)
            {
               newMessage() << "Center failure at term " << i;
               checkValue(result[i], a, error);
            }
            return;
         }

         size_t count(0);
         while (count <= basis.order())
         {
            newMessage() << "Base degree " << count << " order 0 position " << count;
            auto a = boost::math::spherical_harmonic_r(count, 0, theta, phi) * radial(radius, count);
            checkValue(result[count], a, error);
            count++;
         }
         for (int m = 1; m <= basis.order(); m++)
         {
            for (int n = m; n <= basis.order(); n++)
            {
               auto scale = radial(radius, n);
               auto a = scale * boost::math::spherical_harmonic_r(n, m, theta, phi);
               auto b = scale * boost::math::spherical_harmonic_i(n, m, theta, phi);

               newMessage() << "Cosine degree " << n << " order " << m << " position " << count;
               checkValue(result[count], a, error);
               count++;
               newMessage() << "Sine degree " << n << " order " << m << " position " << count;
               checkValue(result[count], b, error);
               count++;
            }
         }
      }

      std::vector<double> m_vPosition;
   private:
      std::stringstream &newMessage()
      {
         m_ssMsg.str("");
         m_ssMsg.clear();
         return m_ssMsg;
      }

      std::stringstream m_ssMsg;
   };

   TEST_F(HarmonicsTest, sphericalHarmonics) // NOLINT
   {
      SphericalHarmonicTest<double> basis;

      checkPosition(basis, [](auto radius, auto)
      { return radius == 0.0 ? 0.0 : 1.0; });
      m_vPosition[0] = 1.0;
      checkPosition(basis, [](auto radius, auto)
      { return radius == 0.0 ? 0.0 : 1.0; });
      m_vPosition[1] = 1.0;
      checkPosition(basis, [](auto radius, auto)
      { return radius == 0.0 ? 0.0 : 1.0; });
      m_vPosition[2] = 1.0;
      checkPosition(basis, [](auto radius, auto)
      { return radius == 0.0 ? 0.0 : 1.0; });
   }

   TEST_F(HarmonicsTest, radial) // NOLINT
   {
      typedef recontools::fit::SolidHarmonic<double> basis_type;
      RadialTest<basis_type> basis;
      basis_type::value_type radius = 10.0;
      basis_type::data_type radial = basis.getRadial(radius);

      for (size_t i = 0; i < radial.size(); i++)
      {
         EXPECT_NEAR(radial[i], pow(radius, i), 1e-15);
      }
   }

   TEST_F(HarmonicsTest, bessel) // NOLINT
   {
      typedef recontools::fit::HelmholtzHarmonic<double> basis_type;
      RadialTest<basis_type> basis;
      basis.wavenumber(basis_type::result_type(1.0, 1.0));
      basis_type::value_type radius = 10.0;
      basis_type::data_type radial = basis.getRadial(radius);

      basis_type::data_type result = {basis_type::result_type(-7.616160677965244e2, -1.624729470287224e2),
                                      basis_type::result_type(1.162684994270754e2, -7.316589124278978e2),
                                      basis_type::result_type(6.693075058464010e2, +0.352838352504763e2),
                                      basis_type::result_type(0.598793358471439e2, +5.731529947789165e2),
                                      basis_type::result_type(-4.477461901272798e2, +1.443619453756440e2),
                                      basis_type::result_type(-1.964022459853801e2, -3.067043338026007e2)};

      for (size_t i = 0; i < radial.size(); i++)
      {
         EXPECT_NEAR(radial[i].real(), result[i].real(), 1e-11) << "Failure at " << i;
         EXPECT_NEAR(radial[i].imag(), result[i].imag(), 1e-11) << "Failure at " << i;
      }
   }

   TEST_F(HarmonicsTest, solidHarmonics) // NOLINT
   {
      recontools::fit::SolidHarmonic<double> basis;
      checkPosition(basis, [](auto radius, auto degree)
      { return pow(radius, degree); });
      m_vPosition[0] = 1.0;
      checkPosition(basis, [](auto radius, auto degree)
      { return pow(radius, degree); });
      m_vPosition[1] = 1.0;
      checkPosition(basis, [](auto radius, auto degree)
      { return pow(radius, degree); });
      m_vPosition[2] = 1.0;
      checkPosition(basis, [](auto radius, auto degree)
      { return pow(radius, degree); });
   }

   TEST_F(HarmonicsTest, helmholtz) // NOLINT
   {
      recontools::fit::HelmholtzHarmonic<double> basis;

      std::vector<recontools::fit::HelmholtzHarmonic<double>::result_type> result(basis.terms(), 0.0);

      basis.compute(m_vPosition.data(), result.data());

      EXPECT_NEAR(result[0].real(), sqrt(0.25 / M_PI), 1e-15);
      EXPECT_NEAR(result[0].imag(), 0.0, 1e-15);
      for (size_t i = 1; i < basis.terms(); i++)
      {
         EXPECT_NEAR(result[i].real(), 0.0, 1e-15);
         EXPECT_NEAR(result[i].imag(), 0.0, 1e-15);
      }
   }

   TEST_F(HarmonicsTest, serializationHelmholtz) // NOLINT
   {
      std::stringstream data;
      recontools::fit::HelmholtzHarmonic<double> basis1;
      basis1.write(data);
      recontools::fit::HelmholtzHarmonic<double> basis2;
      basis2.order(basis1.order() + 1);
      basis2.read(data);

      EXPECT_TRUE(basis1 == basis2);
   }

   TEST_F(HarmonicsTest, serializationSolid) // NOLINT
   {
      std::stringstream data;
      recontools::fit::SolidHarmonic<double> basis1;
      basis1.write(data);
      recontools::fit::SolidHarmonic<double> basis2;
      basis2.order(basis1.order() + 1);
      basis2.read(data);

      EXPECT_TRUE(basis1 == basis2);
   }
}