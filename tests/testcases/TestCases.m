%% ReadInTest
coildata = complex([[1+0i, 1+0i]; [1+0i, 1+0i]; [1+0i, 1+0i]]);
WriteTestCase("ReadInTest.mat", coildata);

%% TwoCoilTest
pointdata = [1+0i; 1+0i; 0+1i];
coildata = [1+0i, 0+1i]; 
imdata = pointdata*coildata;
WriteTestCase("TwoCoilTest.mat", imdata);

%% DataWithNoiseTest
pointdata = [1+0i; 1+0i; 0+1i];
coildata = [1+0i, 0+1i]; 
imdata = pointdata*coildata;
rng(1234)
coildata = awgn(imdata, 50);
WriteTestCase("DataWithNoiseTest.mat", coildata);

%% DataWithLotsOfNoiseTest
coildata = complex([[1+0i, 0+1i]; [1+0i, 0+1i]; [1+0i, 0+1i]]);
rng(1234)
coildata = awgn(coildata, 10);
WriteTestCase("DataWithLotsOfNoiseTest.mat", coildata);

%% PhantomData
load phantomdata.mat
WriteTestCase("PhantomTest.mat", coildata);