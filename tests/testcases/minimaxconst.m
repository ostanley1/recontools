function [ ceq, c ] = minimaxconst( x )
    x=complex(x(1:ceil(length(x)/2)),x(ceil(length(x)/2)+1:end));
    ceq = norm(x(:))-1;
    c=0;
end