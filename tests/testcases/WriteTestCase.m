function name = WriteTestCase(name,coildata)
%     Get solutions for minimax using the optimizer as it runs in the fitted SVD sensitivities method
    options = optimoptions('fminimax', 'MaxFunEvals', 1000*32,...
            'MaxIter', 4000, 'Display', 'iter', 'TolCon', 1e-6,...
            'RelLineSrchBnd', 0.1)
    [~,~,V]=svd(coildata, 'econ');
    x = zeros(size(coildata));
    f = @(w) -abs(coildata*complex(w(1:ceil(length(w)/2)),...
                                   w(ceil(length(w)/2)+1:end)));
    x0 = [real(V(:,1)); imag(V(:,1))];
    [coilweights,~,minval,exitflag] = fminimax(f,x0, [],[],[],[],[],[],@minimaxconst,options);

    N = length(coilweights)/2
    iw=reshape([real(V(:,1)), imag(conj(V(:,1)))]',1,[]); % save SVD solution for intial check
    cd = reshape(coildata.',[],1);
    cd=reshape([real(cd), imag(cd)].',1,[]);
    coilweights = complex(coilweights(1:ceil(length(coilweights)/2)), ...
        coilweights(ceil(length(coilweights)/2)+1:end));
    cw = reshape(coilweights',[],1); % not a dot as the minimax problem is coil by points and here is points by coils
    cw=reshape([real(cw), imag(cw)]',1,[]);
    minval = abs(minval);
    
    save(name, "N", "cd", "cw", "iw", "minval", '-v7.3');
end

