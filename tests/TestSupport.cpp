//
// Created by Martyn Klassen on 2019-12-09.
//

#include "gmock/gmock.h"

#pragma clang diagnostic push
#pragma ide diagnostic ignored "CannotResolve"

#include <recontools/fit/ConvexHull.hpp>
#include <recontools/fit/Ellipsoid.hpp>

namespace
{

   class SupportTest : public ::testing::Test
   {
   protected:
      void SetUp() override
      {
         m_vDims = {4, 4, 4};
         m_vVoxel = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
         m_vCenter = {0.0, 0.0, 0.0};
         m_vOutside = {0.0, 0.0, 2.0};


         m_vMask.resize(m_vDims[0] * m_vDims[1] * m_vDims[2], false);
         m_vMask[1*m_vDims[0]*m_vDims[1] + 1*m_vDims[1] + 1] = true;
         m_vMask[1*m_vDims[0]*m_vDims[1] + 1*m_vDims[1] + 2] = true;
         m_vMask[1*m_vDims[0]*m_vDims[1] + 2*m_vDims[1] + 1] = true;
         m_vMask[1*m_vDims[0]*m_vDims[1] + 2*m_vDims[1] + 2] = true;
         m_vMask[2*m_vDims[0]*m_vDims[1] + 2*m_vDims[1] + 1] = true;
         m_vMask[2*m_vDims[0]*m_vDims[1] + 2*m_vDims[1] + 2] = true;
         m_vMask[2*m_vDims[0]*m_vDims[1] + 1*m_vDims[1] + 1] = true;
         m_vMask[2*m_vDims[0]*m_vDims[1] + 1*m_vDims[1] + 2] = true;

      }

      template<class T>
      void streamTest(T &support)
      {
         std::stringstream data;
         T newSupport;
         EXPECT_FALSE(support == newSupport);
         support.write(data);
         newSupport.read(data);
         EXPECT_TRUE(support == newSupport);
      }

      std::vector<int> m_vDims;
      std::vector<recontools::fit::P3::value_type> m_vCenter;
      std::vector<recontools::fit::P3::value_type> m_vOutside;
      std::vector<recontools::fit::P3::value_type> m_vVoxel;
      recontools::fit::Support::mask_type m_vMask;
   };


   TEST_F(SupportTest, convexHull) // NOLINT
   {
      recontools::fit::ConvexHull convexHull;

      convexHull.computePerimeter(m_vMask, m_vDims.size(), m_vDims.data(), m_vCenter.data(), m_vVoxel.data());

      EXPECT_TRUE(convexHull.interior(m_vCenter.data()));

      EXPECT_FALSE( convexHull.interior(m_vOutside.data()));

      streamTest(convexHull);
   }

   TEST_F(SupportTest, ellipsoid) // NOLINT
   {
      recontools::fit::Ellipsoid ellipsoid;

      ellipsoid.computePerimeter(m_vMask, m_vDims.size(), m_vDims.data(), m_vCenter.data(), m_vVoxel.data());

      EXPECT_TRUE(ellipsoid.interior(m_vCenter.data()));

      EXPECT_FALSE( ellipsoid.interior(m_vOutside.data()));

      streamTest(ellipsoid);
   }

}
